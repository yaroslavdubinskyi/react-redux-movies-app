import React from 'react';
import ReactDOM from 'react-dom';
import Root from './containers/Root';

import '../node_modules/modern-normalize/modern-normalize.css';
import './styles/base.css';

ReactDOM.render(<Root />, document.getElementById('root'));
