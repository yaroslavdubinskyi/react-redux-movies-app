import React from 'react';
import { Helmet } from 'react-helmet';

function NoMatch() {
  return (
    <main className="404-page">
      <Helmet title="No Matches" />
      <div className="container">
        <h2>404</h2>
      </div>
    </main>
  );
}

export default NoMatch;
