import React from 'react';
import { Route, Switch } from 'react-router-dom';
import { Helmet } from 'react-helmet';

import PrivateRoute from '../../components/PrivateRoute';
import AppHeader from '../../components/AppHeader';
import AppFooter from '../../components/AppFooter';
import HomePage from '../HomePage';
import LoginPage from '../LoginPage';
import UserPage from '../UserPage';
import LogoutPage from '../LogoutPage';
import MoviesPage from '../MoviesPage';
import TvShowsPage from '../TvShowsPage';
import SingleMoviePage from '../SingleMoviePage';
import SingleTVShowPage from '../SingleTVShowPage';
import NoMatch from '../NoMatch';

function App() {
  return (
    <div className="App">
      <Helmet defaultTitle="React Redux Movie App" titleTemplate="%s | React Redux Movie App" />
      <AppHeader />
      <Switch>
        <Route exact path="/" component={HomePage} />
        <Route path="/login" component={LoginPage} />
        <PrivateRoute path="/user" component={UserPage} />
        <PrivateRoute path="/logout" component={LogoutPage} />
        <Route path="/movies" component={MoviesPage} />
        <Route path="/tvs" component={TvShowsPage} />
        <Route path="/movie/:movie_id" component={SingleMoviePage} />
        <Route path="/tv/:tv_id" component={SingleTVShowPage} />
        <Route component={NoMatch} />
      </Switch>
      <AppFooter />
    </div>
  );
}

export default App;
