import React from 'react';
import { Helmet } from 'react-helmet';
import { connect } from 'react-redux';
import { CSSTransition } from 'react-transition-group';
import PropTypes from 'prop-types';

import './styles.css';

import { loadTvShows } from '../../actions/tvs';

import ContentGrid from '../../components/ContentGrid';
import GridPagination from '../../components/GridPagination';
import MediaItem from '../../components/MediaItem';

class TvShowsPage extends React.Component {
  componentDidMount() {
    const { page, sortBy, totalPages } = this.props.tvShows;
    if (totalPages === 0) {
      this.props.loadTvShows({ page, sort_by: sortBy });
    }
  }
  render() {
    const { tvShows } = this.props.tvShows;
    const tvShowsList = tvShows.map(item => <MediaItem key={item.id} item={item} />);
    return (
      <main className="grid-page tv-shows-page">
        <Helmet title="Movies" />
        <div className="container">
          <GridPagination
            page={this.props.tvShows.page}
            totalPages={this.props.tvShows.totalPages}
            loadHandler={this.props.loadTvShows}
          />
          <CSSTransition in={!this.props.tvShows.isFetching} timeout={450} classNames="fade-grid">
            <ContentGrid cols="5">{tvShowsList}</ContentGrid>
          </CSSTransition>

          <GridPagination
            page={this.props.tvShows.page}
            totalPages={this.props.tvShows.totalPages}
            loadHandler={this.props.loadTvShows}
          />
        </div>
      </main>
    );
  }
}

TvShowsPage.propTypes = {
  tvShows: PropTypes.shape({
    tvShows: PropTypes.arrayOf(PropTypes.object),
    page: PropTypes.number,
    totalResults: PropTypes.number,
    totalPages: PropTypes.number,
    sortBy: PropTypes.string,
    isFetching: PropTypes.bool,
    isError: PropTypes.bool,
  }).isRequired,
  loadTvShows: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  tvShows: state.tvShows,
});

const mapDispatchToProps = dispatch => ({
  loadTvShows: options => dispatch(loadTvShows(options)),
});

export default connect(mapStateToProps, mapDispatchToProps)(TvShowsPage);
