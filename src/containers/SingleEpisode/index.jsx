import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import MaterialIcon from 'react-google-material-icons';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import ImagesGallery from '../../components/ImagesGallery';

import TMDB from '../../helpers/TMDB';
import scrollToY from '../../helpers/scrollToY';

import './styles.css';

import { getTVEpisode, getTVEpisodeImages } from '../../actions/tvs';

class SingleEpisode extends Component {
  constructor(props) {
    super(props);

    this.singleEpisodeWrap = React.createRef();
  }
  componentDidMount() {
    const tvId = this.props.match.params.tv_id;
    const seasonNumber = this.props.match.params.season_number;
    const episodeNumber = this.props.match.params.episode_number;

    this.props.getTVEpisode(tvId, seasonNumber, episodeNumber).then(() => {
      scrollToY(this.singleEpisodeWrap.current.offsetTop, 1200, 'easeInOutQuint');
    });
    this.props.getTVEpisodeImages(tvId, seasonNumber, episodeNumber);
  }
  componentWillReceiveProps(newProps) {
    if (newProps === undefined) {
      return false;
    }
    const tvId = this.props.match.params.tv_id;
    const newtvId = newProps.match.params.tv_id;

    const seasonNumber = this.props.match.params.season_number;
    const newSeasonNumber = newProps.match.params.season_number;

    const episodeNumber = this.props.match.params.episode_number;
    const newEpisodeNumber = newProps.match.params.episode_number;

    if (
      tvId !== newtvId ||
      seasonNumber !== newSeasonNumber ||
      episodeNumber !== newEpisodeNumber
    ) {
      this.props.getTVEpisodeImages(newtvId, newSeasonNumber, newEpisodeNumber);
      this.props.getTVEpisode(newtvId, newSeasonNumber, newEpisodeNumber).then(() => {
        scrollToY(this.singleEpisodeWrap.current.offsetTop, 1200, 'easeInOutQuint');
      });
    }
  }
  render() {
    const {
      name,
      overview,
      still_path: stillPath,
      air_date: airDate,
      images,
      episode_number: episodeNumber,
    } = this.props.tvEpisode;
    const posterSrc = TMDB.common.getImage({ size: 342, file: stillPath });

    return (
      <section className="single-episode" ref={this.singleEpisodeWrap}>
        <div className="single-episode-description">
          <div className="cols-wrap">
            <div className="poster-col">
              <img src={posterSrc} alt={name} className="poster" />
              <div className="title-wrap">
                <h3>Episode {episodeNumber}</h3>
                <ul className="short-info">
                  <li>
                    <MaterialIcon icon="date_range" /> <span>{airDate}</span>
                  </li>
                </ul>
              </div>
            </div>
            <div className="desc-col">
              <div className="desc-wrap">
                <Link
                  className="close-link"
                  to={`/tv/${this.props.match.params.tv_id}/season/${
                    this.props.match.params.season_number
                  }`}
                >
                  ×
                </Link>
                <h3>{name}</h3>

                <p>{overview || 'Overview will be soon...'}</p>
              </div>
              <ImagesGallery items={images} />
            </div>
          </div>
        </div>
      </section>
    );
  }
}

SingleEpisode.propTypes = {
  tvEpisode: PropTypes.shape({
    name: PropTypes.string,
    overview: PropTypes.string,
    still_path: PropTypes.string,
    air_date: PropTypes.string,
    images: PropTypes.arrayOf(PropTypes.object),
    episode_number: PropTypes.number,
  }).isRequired,
  match: PropTypes.shape({
    params: PropTypes.shape({
      tv_id: PropTypes.node,
      season_number: PropTypes.node,
      episode_number: PropTypes.node,
    }).isRequired,
  }).isRequired,
  getTVEpisode: PropTypes.func.isRequired,
  getTVEpisodeImages: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  tvEpisode: state.singleTV.tvEpisode,
  images: state.singleTV.tvEpisode.images,
});

const mapDispatchToProps = dispatch => ({
  getTVEpisode: (id, seasoneNum, episodeNum) => dispatch(getTVEpisode(id, seasoneNum, episodeNum)),
  getTVEpisodeImages: (id, seasoneNum, episodeNum) =>
    dispatch(getTVEpisodeImages(id, seasoneNum, episodeNum)),
});

export default connect(mapStateToProps, mapDispatchToProps)(SingleEpisode);
