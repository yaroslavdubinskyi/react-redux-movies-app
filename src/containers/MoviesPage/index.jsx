import React from 'react';
import { Helmet } from 'react-helmet';
import { connect } from 'react-redux';
import { CSSTransition } from 'react-transition-group';
import PropTypes from 'prop-types';

import './styles.css';

import { loadMovies } from '../../actions/movies';

import ContentGrid from '../../components/ContentGrid';
import GridPagination from '../../components/GridPagination';
import MediaItem from '../../components/MediaItem';

class MoviesPage extends React.Component {
  componentDidMount() {
    const { page, sortBy, totalPages } = this.props.movies;
    if (totalPages === 0) {
      this.props.loadMovies({ page, sort_by: sortBy });
    }
  }

  render() {
    const { movies } = this.props.movies;
    const moviesList = movies.map(item => <MediaItem key={item.id} item={item} />);
    return (
      <main className="grid-page movies-page">
        <Helmet title="Movies" />
        <div className="container">
          <GridPagination
            page={this.props.movies.page}
            totalPages={this.props.movies.totalPages}
            loadHandler={this.props.loadMovies}
          />
          <CSSTransition in={!this.props.movies.isFetching} timeout={450} classNames="fade-grid">
            <ContentGrid cols="5">{moviesList}</ContentGrid>
          </CSSTransition>

          <GridPagination
            page={this.props.movies.page}
            totalPages={this.props.movies.totalPages}
            loadHandler={this.props.loadMovies}
          />
        </div>
      </main>
    );
  }
}

MoviesPage.propTypes = {
  movies: PropTypes.shape({
    movies: PropTypes.arrayOf(PropTypes.object),
    page: PropTypes.number,
    totalResults: PropTypes.number,
    totalPages: PropTypes.number,
    sortBy: PropTypes.string,
    isFetching: PropTypes.bool,
    isError: PropTypes.bool,
  }).isRequired,
  loadMovies: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  movies: state.movies,
});

const mapDispatchToProps = dispatch => ({
  loadMovies: options => dispatch(loadMovies(options)),
});

export default connect(mapStateToProps, mapDispatchToProps)(MoviesPage);
