import React from 'react';
import { Helmet } from 'react-helmet';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import scrollToY from '../../helpers/scrollToY';

import {
  getMovieDetails,
  getMovieAccountStates,
  getMovieRecomendations,
  getMovieSimilars,
  rateMovie,
} from '../../actions/movies';
import { markAsFavorite } from '../../actions/user';

import './styles.css';

import SingleMovieDetails from '../../components/SingleMovieDetails';
import ContentGrid from '../../components/ContentGrid';
import MediaItem from '../../components/MediaItem';

class SingleMoviePage extends React.Component {
  constructor(props) {
    super(props);

    this.favoriteMovieHandler = this.favoriteMovieHandler.bind(this);
    this.rateHandler = this.rateHandler.bind(this);
  }

  componentDidMount() {
    scrollToY(0, 1500, 'easeInOutQuint');
    const movieId = this.props.match.params.movie_id;
    this.props.getMovieDetails(movieId);
    if (this.props.isLoggedIn) {
      this.props.getMovieAccountStates(movieId, this.props.sessionId);
    }
    this.props.getMovieRecomendations(movieId);
    this.props.getMovieSimilars(movieId);
  }

  componentWillReceiveProps(newProps) {
    if (newProps === undefined) {
      return false;
    }
    const movieId = this.props.match.params.movie_id;
    const newMovieId = newProps.match.params.movie_id;
    if (movieId !== newMovieId) {
      scrollToY(0, 1500, 'easeInOutQuint');
      this.props.getMovieDetails(newMovieId);
      if (this.props.isLoggedIn) {
        this.props.getMovieAccountStates(newMovieId, this.props.sessionId);
      }
      this.props.getMovieRecomendations(newMovieId);
      this.props.getMovieSimilars(newMovieId);
    }
  }

  favoriteMovieHandler() {
    this.props
      .markAsFavorite({
        id: this.props.userId,
        session_id: this.props.sessionId,
        data: {
          media_type: 'movie',
          media_id: this.props.movieDetails.id,
          favorite: !this.props.accountStates.favorite,
        },
      })
      .then(() => {
        this.props.getMovieAccountStates(this.props.movieDetails.id, this.props.sessionId);
      });
  }

  rateHandler(rate) {
    this.props.rateMovie({
      id: this.props.movieDetails.id,
      session_id: this.props.sessionId,
      data: {
        value: rate / 10,
      },
    });
  }

  render() {
    const recomendsList = this.props.movieRecomendations
      .slice(0, 6)
      .map(item => <MediaItem item={item} key={item.id} imgWidth="185" />);
    const similarsList = this.props.movieSimilars
      .slice(0, 6)
      .map(item => <MediaItem item={item} key={item.id} imgWidth="185" />);
    return (
      <main className="single-movie-page">
        <Helmet title={this.props.movieDetails.title} />
        <SingleMovieDetails
          item={this.props.movieDetails}
          accountStates={this.props.accountStates}
          favoriteMovieHandler={this.favoriteMovieHandler}
          rateHandler={this.rateHandler}
          isLoggedIn={this.props.isLoggedIn}
        />
        <div className="container">
          <h2 className="section-title">Recomendations</h2>
          <ContentGrid cols="6">{recomendsList}</ContentGrid>
          <h2 className="section-title">Similar movies</h2>
          <ContentGrid cols="6">{similarsList}</ContentGrid>
        </div>
      </main>
    );
  }
}

SingleMoviePage.propTypes = {
  movieDetails: PropTypes.shape({
    id: PropTypes.number,
    title: PropTypes.string,
    tagline: PropTypes.string,
    overview: PropTypes.string,
    release_date: PropTypes.string,
    backdrop_path: PropTypes.string,
    poster_path: PropTypes.string,
    budget: PropTypes.number,
    vote_average: PropTypes.number,
    vote_count: PropTypes.number,
  }).isRequired,
  accountStates: PropTypes.shape({
    favorite: PropTypes.bool,
    rated: PropTypes.oneOfType([PropTypes.object, PropTypes.bool]),
  }).isRequired,
  movieRecomendations: PropTypes.arrayOf(PropTypes.object).isRequired,
  movieSimilars: PropTypes.arrayOf(PropTypes.object).isRequired,
  sessionId: PropTypes.string.isRequired,
  userId: PropTypes.number.isRequired,
  isLoggedIn: PropTypes.bool,
  match: PropTypes.shape({
    params: PropTypes.shape({
      movie_id: PropTypes.node,
    }).isRequired,
  }).isRequired,
  getMovieDetails: PropTypes.func.isRequired,
  getMovieAccountStates: PropTypes.func.isRequired,
  getMovieRecomendations: PropTypes.func.isRequired,
  getMovieSimilars: PropTypes.func.isRequired,
  markAsFavorite: PropTypes.func.isRequired,
  rateMovie: PropTypes.func.isRequired,
};

SingleMoviePage.defaultProps = {
  isLoggedIn: false,
};

const mapStateToProps = state => ({
  movieDetails: state.singleMovie.movieDetails,
  accountStates: state.singleMovie.accountStates,
  movieRecomendations: state.singleMovie.movieRecomendations,
  movieSimilars: state.singleMovie.movieSimilars,
  sessionId: state.auth.sessionID,
  userId: state.user.userID,
  isLoggedIn: state.auth.isLoggedIn,
});

const mapDispatchToProps = dispatch => ({
  getMovieDetails: id => dispatch(getMovieDetails(id)),
  getMovieAccountStates: (id, sessionId) =>
    dispatch(getMovieAccountStates({ id, session_id: sessionId })),
  getMovieRecomendations: id => dispatch(getMovieRecomendations(id)),
  getMovieSimilars: id => dispatch(getMovieSimilars(id)),
  markAsFavorite: options => dispatch(markAsFavorite(options)),
  rateMovie: options => dispatch(rateMovie(options)),
});

export default connect(mapStateToProps, mapDispatchToProps)(SingleMoviePage);
