import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import PropTypes from 'prop-types';

import { generateToken, generateSession } from '../../actions/auth';

import './style.css';

class LoginPage extends Component {
  constructor(props) {
    super(props);

    const urlString = window.location.href;
    const url = new URL(urlString);
    const approved = url.searchParams.get('approved');
    const requestToken = url.searchParams.get('request_token');

    if (approved) {
      this.props.generateSession(requestToken);

      // setTimeout(() => {
      //   window.close();
      // }, 200);
    }
    this.askPermissions = this.askPermissions().bind(this);
  }

  askPermissions() {
    window.open(
      `https://www.themoviedb.org/authenticate/${this.props.auth.requestTokken}?redirect_to=${
        window.location.href
      }`,
      'targetWindow',
      'toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=1100,height=6000',
    );
  }
  render() {
    return (
      <main className="login-page">
        <Helmet title="Login" />
        <div className="container">
          <h1>This is login page</h1>
          <button onClick={this.props.generateToken}>generateToken</button>
          <button onClick={this.askPermissions}>askPermissions</button>
          <button onClick={() => this.props.generateSession(this.props.auth.requestTokken)}>
            generateSession
          </button>
          <br />
          <hr />
          <table>
            <tbody>
              <tr>
                <td>requestTokken:</td>
                <td>{this.props.auth.requestTokken}</td>
              </tr>
              <tr>
                <td>sessionID:</td>
                <td>{this.props.auth.sessionID}</td>
              </tr>
            </tbody>
          </table>
        </div>
      </main>
    );
  }
}

LoginPage.propTypes = {
  auth: PropTypes.shape({
    requestTokken: PropTypes.string,
    isLoggedIn: PropTypes.bool,
    isFetching: PropTypes.bool,
    isError: PropTypes.bool,
    sessionID: PropTypes.string,
  }).isRequired,
  generateToken: PropTypes.func.isRequired,
  generateSession: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  auth: state.auth,
});

const mapDispatchToProps = dispatch => ({
  generateToken: () => dispatch(generateToken()),
  generateSession: token => dispatch(generateSession(token)),
});

export default connect(mapStateToProps, mapDispatchToProps)(LoginPage);
