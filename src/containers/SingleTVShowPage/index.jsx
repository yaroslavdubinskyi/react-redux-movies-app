import React from 'react';
import { Route } from 'react-router-dom';
import { Helmet } from 'react-helmet';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import scrollToY from '../../helpers/scrollToY';

import {
  getTVDetails,
  getTVAccountStates,
  getTVRecomendations,
  getTVSimilars,
  rateTV,
} from '../../actions/tvs';
import { markAsFavorite } from '../../actions/user';

import './styles.css';

import SingleSeason from '../SingleSeason';

import SingleTVDetails from '../../components/SingleTVDetails';
import ContentGrid from '../../components/ContentGrid';
import MediaItem from '../../components/MediaItem';
import SeasonItem from '../../components/SeasonItem';

class SingleTVShowPage extends React.Component {
  constructor(props) {
    super(props);

    this.favoriteTVHandler = this.favoriteTVHandler.bind(this);
    this.rateHandler = this.rateHandler.bind(this);
  }

  componentDidMount() {
    scrollToY(0, 1500, 'easeInOutQuint');
    const tvId = this.props.match.params.tv_id;
    this.props.getTVDetails(tvId);
    if (this.props.isLoggedIn) {
      this.props.getTVAccountStates(tvId, this.props.sessionId);
    }
    this.props.getTVRecomendations(tvId);
    this.props.getTVSimilars(tvId);
  }
  componentWillReceiveProps(newProps) {
    if (newProps === undefined) {
      return false;
    }
    const tvId = this.props.match.params.tv_id;
    const newtvId = newProps.match.params.tv_id;
    if (tvId !== newtvId) {
      scrollToY(0, 1500, 'easeInOutQuint');
      this.props.getTVDetails(newtvId);
      if (this.props.isLoggedIn) {
        this.props.getTVAccountStates(newtvId, this.props.sessionId);
      }
      this.props.getTVRecomendations(newtvId);
      this.props.getTVSimilars(newtvId);
    }
  }

  favoriteTVHandler() {
    this.props
      .markAsFavorite({
        id: this.props.userId,
        session_id: this.props.sessionId,
        data: {
          media_type: 'tv',
          media_id: this.props.tvDetails.id,
          favorite: !this.props.accountStates.favorite,
        },
      })
      .then(() => {
        this.props.getTVAccountStates(this.props.tvDetails.id, this.props.sessionId);
      });
  }

  rateHandler(rate) {
    this.props.rateTV({
      id: this.props.tvDetails.id,
      session_id: this.props.sessionId,
      data: {
        value: rate / 10,
      },
    });
  }

  render() {
    const recomendsList = this.props.tvRecomendations
      .slice(0, 6)
      .map(item => <MediaItem item={item} key={item.id} />);
    const similarsList = this.props.tvSimilars
      .slice(0, 6)
      .map(item => <MediaItem item={item} key={item.id} />);

    const seasonsList = this.props.tvDetails.seasons.map(item => (
      <SeasonItem tvId={this.props.match.params.tv_id} item={item} key={item.id} />
    ));
    return (
      <main className="single-tv-show-page">
        <Helmet title={this.props.tvDetails.name} />
        <SingleTVDetails
          item={this.props.tvDetails}
          accountStates={this.props.accountStates}
          favoriteTVHandler={this.favoriteTVHandler}
          rateHandler={this.rateHandler}
          isLoggedIn={this.props.isLoggedIn}
        />
        <div className="container">
          <h2 className="section-title">Seasons</h2>
          <ContentGrid cols="6">{seasonsList}</ContentGrid>

          <Route path="/tv/:tv_id/season/:season_number" component={SingleSeason} />

          <h2 className="section-title">Recomendations</h2>
          <ContentGrid cols="6">{recomendsList}</ContentGrid>
          <h2 className="section-title">Similar TV Shows</h2>
          <ContentGrid cols="6">{similarsList}</ContentGrid>
        </div>
      </main>
    );
  }
}

SingleTVShowPage.propTypes = {
  tvDetails: PropTypes.shape({
    id: PropTypes.number,
    name: PropTypes.string,
    seasons: PropTypes.arrayOf(PropTypes.object),
  }).isRequired,
  accountStates: PropTypes.shape({
    favorite: PropTypes.bool,
    rated: PropTypes.oneOfType([PropTypes.object, PropTypes.bool]),
  }).isRequired,
  tvRecomendations: PropTypes.arrayOf(PropTypes.object).isRequired,
  tvSimilars: PropTypes.arrayOf(PropTypes.object).isRequired,
  sessionId: PropTypes.string.isRequired,
  userId: PropTypes.number.isRequired,
  isLoggedIn: PropTypes.bool,
  match: PropTypes.shape({
    params: PropTypes.shape({
      tv_id: PropTypes.node,
    }).isRequired,
  }).isRequired,
  getTVDetails: PropTypes.func.isRequired,
  getTVAccountStates: PropTypes.func.isRequired,
  getTVRecomendations: PropTypes.func.isRequired,
  getTVSimilars: PropTypes.func.isRequired,
  markAsFavorite: PropTypes.func.isRequired,
  rateTV: PropTypes.func.isRequired,
};

SingleTVShowPage.defaultProps = {
  isLoggedIn: false,
};

const mapStateToProps = state => ({
  tvDetails: state.singleTV.tvDetails,
  accountStates: state.singleTV.accountStates,
  tvRecomendations: state.singleTV.tvRecomendations,
  tvSimilars: state.singleTV.tvSimilars,
  sessionId: state.auth.sessionID,
  userId: state.user.userID,
  isLoggedIn: state.auth.isLoggedIn,
});

const mapDispatchToProps = dispatch => ({
  getTVDetails: id => dispatch(getTVDetails(id)),
  getTVAccountStates: (id, sessionId) =>
    dispatch(getTVAccountStates({ id, session_id: sessionId })),
  getTVRecomendations: id => dispatch(getTVRecomendations(id)),
  getTVSimilars: id => dispatch(getTVSimilars(id)),
  markAsFavorite: options => dispatch(markAsFavorite(options)),
  rateTV: options => dispatch(rateTV(options)),
});

export default connect(mapStateToProps, mapDispatchToProps)(SingleTVShowPage);
