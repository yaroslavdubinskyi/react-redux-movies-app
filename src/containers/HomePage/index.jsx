import React from 'react';
import { Helmet } from 'react-helmet';

function HomePage() {
  return (
    <main className="home-page">
      <Helmet title="Home" />
      <div className="container">
        <h2>This is Home page</h2>
      </div>
    </main>
  );
}
export default HomePage;
