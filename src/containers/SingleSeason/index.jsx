import React, { Component } from 'react';
import { Link, Route } from 'react-router-dom';
import MaterialIcon from 'react-google-material-icons';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import TMDB from '../../helpers/TMDB';
import scrollToY from '../../helpers/scrollToY';

import ContentGrid from '../../components/ContentGrid';
import EpisodeItem from '../../components/EpisodeItem';
import SingleEpisode from '../SingleEpisode';

import './styles.css';

import { getTVSeason } from '../../actions/tvs';

class SingleSeason extends Component {
  constructor(props) {
    super(props);

    this.singleSeasoneWrap = React.createRef();
  }
  componentDidMount() {
    const tvId = this.props.match.params.tv_id;
    const seasonNumber = this.props.match.params.season_number;
    this.props.getTVSeason(tvId, seasonNumber).then(() => {
      scrollToY(this.singleSeasoneWrap.current.offsetTop, 1500, 'easeInOutQuint');
    });
  }
  componentWillReceiveProps(newProps) {
    if (newProps === undefined) {
      return false;
    }
    const tvId = this.props.match.params.tv_id;
    const newtvId = newProps.match.params.tv_id;

    const seasonNumber = this.props.match.params.season_number;
    const newSeasonNumber = newProps.match.params.season_number;
    if (tvId !== newtvId || seasonNumber !== newSeasonNumber) {
      this.props.getTVSeason(newtvId, newSeasonNumber).then(() => {
        scrollToY(this.singleSeasoneWrap.current.offsetTop, 1500, 'easeInOutQuint');
      });
    }
  }
  render() {
    const {
      name,
      overview,
      poster_path: posterPath,
      air_date: airDate,
      episodes,
    } = this.props.tvSeason;
    const tvId = this.props.match.params.tv_id;
    const posterSrc = TMDB.common.getImage({ size: 500, file: posterPath });

    const episodesList = episodes.map(item => (
      <EpisodeItem tvId={tvId} item={item} key={item.id} />
    ));

    return (
      <section className="single-season" ref={this.singleSeasoneWrap}>
        <div className="single-season-description">
          <div className="cols-wrap">
            <div className="poster-col">
              <img src={posterSrc} alt={name} className="poster" />
              <div className="title-wrap">
                <h3>{name}</h3>
                <ul className="short-info">
                  <li>
                    <MaterialIcon icon="date_range" /> <span>{airDate}</span>
                  </li>
                </ul>
              </div>
            </div>
            <div className="desc-col">
              <div className="desc-wrap">
                <Link className="close-link" to={`/tv/${this.props.match.params.tv_id}`}>
                  ×
                </Link>

                <p>{overview || 'Overview will be soon...'}</p>
              </div>
              <div className="single-season-episodes">
                <ContentGrid>{episodesList}</ContentGrid>
              </div>
            </div>
          </div>
        </div>
        <Route
          path="/tv/:tv_id/season/:season_number/episode/:episode_number"
          component={SingleEpisode}
        />
      </section>
    );
  }
}

SingleSeason.propTypes = {
  tvSeason: PropTypes.shape({
    name: PropTypes.string,
    overview: PropTypes.string,
    poster_path: PropTypes.string,
    air_date: PropTypes.string,
    episodes: PropTypes.arrayOf(PropTypes.object),
  }).isRequired,
  match: PropTypes.shape({
    params: PropTypes.shape({
      tv_id: PropTypes.node,
      season_number: PropTypes.node,
    }).isRequired,
  }).isRequired,
  getTVSeason: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  tvSeason: state.singleTV.tvSeason,
});

const mapDispatchToProps = dispatch => ({
  getTVSeason: (id, number) => dispatch(getTVSeason(id, number)),
});

export default connect(mapStateToProps, mapDispatchToProps)(SingleSeason);
