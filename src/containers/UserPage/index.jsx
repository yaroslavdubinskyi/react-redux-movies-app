import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import PropTypes from 'prop-types';

import './styles.css';

import {
  getUserDetails,
  getUserFavoriteMovies,
  getUserFavoriteTvShows,
  getUserRatedMovies,
  getUserRatedTvShows,
} from '../../actions/user';

import MediaItem from '../../components/MediaItem';
import ContentGrid from '../../components/ContentGrid';

class UserPage extends Component {
  componentDidMount() {
    this.props.getUserDetails(this.props.sessionId).then(() => {
      this.props.getUserFavoriteMovies(this.props.user.userID, this.props.sessionId);
      this.props.getUserFavoriteTvShows(this.props.user.userID, this.props.sessionId);
      this.props.getUserRatedMovies(this.props.user.userID, this.props.sessionId);
      this.props.getUserRatedTvShows(this.props.user.userID, this.props.sessionId);
    });
  }
  render() {
    const {
      userDetails,
      favoriteMovies,
      favoriteTvShows,
      ratedMovies,
      ratedTvShows,
    } = this.props.user;

    const favoriteMoviesList = favoriteMovies.map(item => <MediaItem key={item.id} item={item} />);
    const favoriteTvShowsList = favoriteTvShows.map(item => (
      <MediaItem key={item.id} item={item} />
    ));

    const ratedMoviesList = ratedMovies.map(item => <MediaItem key={item.id} item={item} />);

    const ratedTvShowsList = ratedTvShows.map(item => <MediaItem key={item.id} item={item} />);

    return (
      <main className="user-page">
        <Helmet title="User" />
        <div className="container">
          <section className="user-details">
            <div className="user-details-cols">
              <div className="avatar-col">
                <div className="avatar-wrap">
                  <img
                    src={`https://www.gravatar.com/avatar/${
                      userDetails.avatar.gravatar.hash
                    }?s=500`}
                    alt={userDetails.name}
                  />
                </div>
              </div>
              <div className="name-col">
                <div className="name-wrap">
                  <h2>
                    {userDetails.name} ({userDetails.username})
                  </h2>
                </div>
              </div>
            </div>
          </section>
          <section className="favorite-movies-section user-page-section">
            <h3 className="section-title">Rated Movies</h3>
            <ContentGrid cols="6">{ratedMoviesList}</ContentGrid>
          </section>
          <section className="favorite-movies-section user-page-section">
            <h3 className="section-title">Rated TV shows</h3>
            <ContentGrid cols="6">{ratedTvShowsList}</ContentGrid>
          </section>
          <section className="favorite-movies-section user-page-section">
            <h3 className="section-title">Favorite Movies</h3>
            <ContentGrid cols="6">{favoriteMoviesList}</ContentGrid>
          </section>
          <section className="favorite-movies-section user-page-section">
            <h3 className="section-title">Favorite TV shows</h3>
            <ContentGrid cols="6">{favoriteTvShowsList}</ContentGrid>
          </section>
        </div>
      </main>
    );
  }
}

UserPage.propTypes = {
  user: PropTypes.shape({
    userID: PropTypes.string,
    userDetails: PropTypes.shape({
      id: PropTypes.string,
      name: PropTypes.string,
      username: PropTypes.string,
    }),
    ratedMovies: PropTypes.arrayOf(PropTypes.object),
    ratedTvShows: PropTypes.arrayOf(PropTypes.object),
    favoriteMovies: PropTypes.arrayOf(PropTypes.object),
    favoriteTvShows: PropTypes.arrayOf(PropTypes.object),
  }).isRequired,
  sessionId: PropTypes.string.isRequired,
  getUserDetails: PropTypes.func.isRequired,
  getUserFavoriteMovies: PropTypes.func.isRequired,
  getUserFavoriteTvShows: PropTypes.func.isRequired,
  getUserRatedMovies: PropTypes.func.isRequired,
  getUserRatedTvShows: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  user: state.user,
  sessionId: state.auth.sessionID,
});

const mapDispatchToProps = dispatch => ({
  getUserDetails: sessionId => dispatch(getUserDetails({ session_id: sessionId })),
  getUserFavoriteMovies: (userId, sessionId) =>
    dispatch(getUserFavoriteMovies({ id: userId, session_id: sessionId })),
  getUserFavoriteTvShows: (userId, sessionId) =>
    dispatch(getUserFavoriteTvShows({ id: userId, session_id: sessionId })),
  getUserRatedMovies: (userId, sessionId) =>
    dispatch(getUserRatedMovies({ id: userId, session_id: sessionId })),
  getUserRatedTvShows: (userId, sessionId) =>
    dispatch(getUserRatedTvShows({ id: userId, session_id: sessionId })),
});

export default connect(mapStateToProps, mapDispatchToProps)(UserPage);
