import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { logoutUser } from '../../actions/auth';

class LogoutPage extends Component {
  componentDidMount() {
    this.props.logoutUser();
  }
  render() {
    return (
      <main className="logout-page">
        <div className="container">
          <h2>Logout page</h2>
        </div>
      </main>
    );
  }
}

LogoutPage.propTypes = {
  logoutUser: PropTypes.func.isRequired,
};

const mapDispatchToProps = dispatch => ({
  logoutUser: () => dispatch(logoutUser()),
});

export default connect(null, mapDispatchToProps)(LogoutPage);
