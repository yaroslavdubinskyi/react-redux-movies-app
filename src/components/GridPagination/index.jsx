import React from 'react';
import PropTypes from 'prop-types';

import './style.css';

function GridPagination(props) {
  const prevPage = props.page - 1;
  const nextPage = props.page + 1;
  return (
    <div className="grid-pagination">
      {props.page > 2 && (
        <button className="first pag-button" onClick={() => props.loadHandler({ page: 1 })}>
          First
        </button>
      )}
      {prevPage > 0 ? (
        <button className="prev pag-button" onClick={() => props.loadHandler({ page: prevPage })}>
          Previous
        </button>
      ) : (
        <button className="prev pag-button" disabled>
          Previous
        </button>
      )}

      <span className="pag-info">
        {props.page} of {props.totalPages}
      </span>
      {nextPage < props.totalPages ? (
        <button className="next pag-button" onClick={() => props.loadHandler({ page: nextPage })}>
          Next
        </button>
      ) : (
        <button className="next pag-button" disabled>
          Next
        </button>
      )}
      {props.page < props.totalPages - 2 && (
        <button
          className="last pag-button"
          onClick={() => props.loadHandler({ page: props.totalPages })}
        >
          Last
        </button>
      )}
    </div>
  );
}

GridPagination.propTypes = {
  page: PropTypes.number.isRequired,
  totalPages: PropTypes.number.isRequired,
  loadHandler: PropTypes.func.isRequired,
};

export default GridPagination;
