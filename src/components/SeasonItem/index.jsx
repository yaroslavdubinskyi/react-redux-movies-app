import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

import TMDB from '../../helpers/TMDB';

import './styles.css';

function SeasonItem({ tvId, item }) {
  const seasonNumber = item.season_number;
  const posterPath = item.poster_path;
  const posterSrc = TMDB.common.getImage({ size: 342, file: posterPath });
  return (
    <article className="season-item">
      <Link to={`/tv/${tvId}/season/${seasonNumber}`}>
        <img src={posterSrc} alt={`Season #${seasonNumber}`} />
      </Link>
    </article>
  );
}

SeasonItem.propTypes = {
  tvId: PropTypes.string.isRequired,
  item: PropTypes.shape({
    season_number: PropTypes.number,
    poster_path: PropTypes.string,
  }).isRequired,
};

export default SeasonItem;
