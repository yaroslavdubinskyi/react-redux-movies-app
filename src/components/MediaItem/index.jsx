import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

import TMDB from '../../helpers/TMDB';

import RatingBadge from '../RatingBadge';
import UserRateBadge from '../UserRateBadge';

import './styles.css';

function MediaItem(props) {
  const { id } = props.item;
  const title = props.item.title ? props.item.title : props.item.name;
  const link = props.item.title ? `/movie/${id}` : `/tv/${id}`;
  const posterWidth = props.imgWidth;
  const posterUrl = props.item.poster_path;
  const posterSrc = TMDB.common.getImage({ size: posterWidth, file: posterUrl });
  const rating = props.item.vote_average;
  const vouteCount = props.item.vote_count;
  const userRate = props.item.rating ? props.item.rating : 0;
  return (
    <article className="media-item">
      <Link to={link}>
        <img src={posterSrc} alt={title} />
        <div className="desc-wrap">
          <p className="title">{title}</p>
        </div>
        <RatingBadge rating={rating} count={vouteCount} />
        {userRate !== 0 && <UserRateBadge rate={userRate} />}
      </Link>
    </article>
  );
}

MediaItem.propTypes = {
  item: PropTypes.shape({
    id: PropTypes.number,
    name: PropTypes.string,
    title: PropTypes.string,
    poster_path: PropTypes.string,
    vote_average: PropTypes.number,
    vote_count: PropTypes.number,
    rating: PropTypes.number,
  }).isRequired,
  imgWidth: PropTypes.string,
};

MediaItem.defaultProps = {
  imgWidth: '342',
};

export default MediaItem;
