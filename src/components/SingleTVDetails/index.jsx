import React from "react";
import MaterialIcon from "react-google-material-icons";
import PropTypes from "prop-types";

import TMDB from "../../helpers/TMDB";

import RatingBadge from "../RatingBadge";
import FavoriteButton from "../FavoriteButton";
import RateButton from "../RateButton";

import "./styles.css";

SingleTVDetails.propTypes = {
  item: PropTypes.shape({
    name: PropTypes.string,
    overview: PropTypes.string,
    tagline: PropTypes.string,
    first_air_date: PropTypes.string,
    last_air_date: PropTypes.string,
    number_of_episodes: PropTypes.number,
    number_of_seasons: PropTypes.number,
    vote_average: PropTypes.number,
    vote_count: PropTypes.number,
    runtime: PropTypes.number,
    poster_path: PropTypes.string,
    backdrop_path: PropTypes.string,
    rating: PropTypes.number
  }),
  accountStates: PropTypes.object,
  favoriteTVHandler: PropTypes.func,
  rateHandler: PropTypes.func
};

function SingleTVDetails({
  item,
  accountStates,
  favoriteTVHandler,
  rateHandler,
  isLoggedIn
}) {
  const title = item.name;
  const overview = item.overview;
  const tagline = item.tagline;
  const firstAirDate = item.first_air_date;
  const lastAirDate = item.last_air_date;
  const numberOfEpisodes = item.number_of_episodes;
  const numberOfSeasons = item.number_of_seasons;
  const rating = item.vote_average;
  const vouteCount = item.vote_count;
  const posterSrc = TMDB.common.getImage({ size: 342, file: item.poster_path });
  const backdropSrc = TMDB.common.getImage({
    size: 780,
    file: item.backdrop_path
  });
  return (
    <section className="single-tv-show-details">
      <img src={backdropSrc} alt="Backdrop" className="backdrop-bg" />
      <div className="container">
        <div className="cols-wrap">
          <div className="poster-col">
            <img src={posterSrc} alt={title} className="poster" />
          </div>
          <div className="desc-col">
            <div className="desc-wrap">
              <h2>{title}</h2>
              <p className="tagline">{tagline}</p>
              <ul className="short-info">
                <li>
                  <MaterialIcon icon="date_range" />{" "}
                  <span>
                    {firstAirDate} - {lastAirDate}
                  </span>
                </li>
                <li>
                  <strong>Seasons:</strong> <span>{numberOfSeasons}</span>
                </li>
                <li>
                  <strong>Episodes:</strong> <span>{numberOfEpisodes}</span>
                </li>
              </ul>
              <p>{overview}</p>
            </div>
          </div>
          <div className="rating-col">
            <RatingBadge rating={rating} count={vouteCount} />
            {isLoggedIn && (
              <React.Fragment>
                <FavoriteButton
                  favorite={accountStates.favorite}
                  favoriteHandler={favoriteTVHandler}
                />
                <RateButton
                  rate={accountStates.rated}
                  rateHandler={rateHandler}
                />
              </React.Fragment>
            )}
          </div>
        </div>
      </div>
    </section>
  );
}

export default SingleTVDetails;
