import React from 'react';

import './styles.css';

function AppFooter() {
  return (
    <footer className="app-footer">
      <p className="copyrights">Created using TMDB</p>
    </footer>
  );
}

export default AppFooter;
