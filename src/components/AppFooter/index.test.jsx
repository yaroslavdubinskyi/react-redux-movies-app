import React from 'react';
import { shallow } from 'enzyme';

import AppFooter from './index';

describe('<AppFooter />', () => {
  it('should render an <footer> tag', () => {
    const renderedComponent = shallow(<AppFooter />);
    expect(renderedComponent.type()).toEqual('footer');
  });
});
