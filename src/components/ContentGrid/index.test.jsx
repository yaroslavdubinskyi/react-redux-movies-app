import React from 'react';
import { shallow } from 'enzyme';

import ContentGrid from './index';

describe('<ContentGrid />', () => {
  it('should render an <section> tag', () => {
    const renderedComponent = shallow(
      <ContentGrid>
        <p>test</p>
      </ContentGrid>,
    );
    expect(renderedComponent.type()).toEqual('section');
  });

  it('should have a className attribute', () => {
    const renderedComponent = shallow(
      <ContentGrid>
        <p>test</p>
      </ContentGrid>,
    );
    expect(renderedComponent.prop('className')).toBeDefined();
  });
});
