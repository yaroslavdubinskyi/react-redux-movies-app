import React from 'react';
import PropTypes from 'prop-types';

import './styles.css';

function ContentGrid(props) {
  const cols = props.cols ? props.cols : 4;

  const classes = `content-grid cols-${cols}`;
  return <section className={classes}>{props.children}</section>;
}

ContentGrid.propTypes = {
  cols: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  children: PropTypes.element.isRequired,
};

ContentGrid.defaultProps = {
  cols: 'auto',
};

export default ContentGrid;
