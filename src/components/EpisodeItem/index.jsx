import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

import TMDB from '../../helpers/TMDB';

import './styles.css';

function EpisodeItem({ item, tvId }) {
  const { name } = item.name;
  const airDate = item.air_date;
  const episodeNumber = item.episode_number;
  const seasonNumber = item.season_number;
  const posterPath = item.still_path;
  const posterSrc = TMDB.common.getImage({ size: 300, file: posterPath });
  return (
    <article className="episode-item">
      <Link to={`/tv/${tvId}/season/${seasonNumber}/episode/${episodeNumber}`}>
        <img src={posterSrc} alt={name} />
        <div className="description-wrap">
          <p>{name}</p>
          <p>{airDate}</p>
        </div>
      </Link>
    </article>
  );
}

EpisodeItem.propTypes = {
  item: PropTypes.shape({
    name: PropTypes.string,
    air_date: PropTypes.string,
    episode_number: PropTypes.number,
    seasonNumber: PropTypes.number,
    still_path: PropTypes.string,
  }).isRequired,
  tvId: PropTypes.string.isRequired,
};

export default EpisodeItem;
