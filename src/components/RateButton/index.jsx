import React, { Component } from 'react';
import PropTypes from 'prop-types';
import MaterialIcon from 'react-google-material-icons';

import './styles.css';

class RateButton extends Component {
  constructor(props) {
    super(props);
    this.state = {
      opened: false,
      rate: this.props.rate === false ? 0 : this.props.rate.value * 10,
      userRate: this.props.rate === false ? 0 : this.props.rate.value * 10,
    };

    this.starEnterhandler = this.starEnterhandler.bind(this);
    this.toggleStars = this.toggleStars.bind(this);
    this.starClickHandler = this.starClickHandler.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.rate !== this.state.rate || nextProps.rate !== this.state.userRate) {
      this.setState({
        rate: nextProps.rate === false ? 0 : nextProps.rate.value * 10,
        userRate: nextProps.rate === false ? 0 : nextProps.rate.value * 10,
      });
    }
  }

  toggleStars() {
    this.setState({
      opened: !this.state.opened,
    });
  }
  starEnterhandler(rate) {
    this.setState({
      rate,
    });
  }

  starClickHandler() {
    this.props.rateHandler(this.state.rate);
    this.setState({
      opened: false,
      userRate: this.state.rate,
    });
  }

  render() {
    const hoverStarsStyles = {
      width: `${this.state.rate}%`,
    };
    const rateStarsWrapClass = this.state.opened ? 'rate-stars-wrap opened' : 'rate-stars-wrap';
    return (
      <div className="rate-button-wrap">
        <button className="rate-button" onClick={this.toggleStars}>
          {this.state.userRate === 0 ? (
            <MaterialIcon icon="star_border" />
          ) : (
            <React.Fragment>
              <MaterialIcon icon="star" />
              <span>{this.state.userRate / 10}</span>
            </React.Fragment>
          )}
        </button>
        <div className={rateStarsWrapClass}>
          <div className="bg-stars">
            <div className="rate-star" onMouseEnter={() => this.starEnterhandler(20)}>
              <MaterialIcon icon="star_border" />
            </div>
            <div className="rate-star" onMouseEnter={() => this.starEnterhandler(40)}>
              <MaterialIcon icon="star_border" />
            </div>
            <div className="rate-star" onMouseEnter={() => this.starEnterhandler(60)}>
              <MaterialIcon icon="star_border" />
            </div>
            <div className="rate-star" onMouseEnter={() => this.starEnterhandler(80)}>
              <MaterialIcon icon="star_border" />
            </div>
            <div className="rate-star" onMouseEnter={() => this.starEnterhandler(100)}>
              <MaterialIcon icon="star_border" />
            </div>
          </div>
          <div className="hovered-stars" style={hoverStarsStyles}>
            <div
              className="rate-star"
              onMouseEnter={() => this.starEnterhandler(20)}
              onClick={this.starClickHandler}
              onKeyPress={() => {}}
              role="button"
              tabIndex="0"
            >
              <MaterialIcon icon="star" />
            </div>
            <div
              className="rate-star"
              onMouseEnter={() => this.starEnterhandler(40)}
              onClick={this.starClickHandler}
              onKeyPress={() => {}}
              role="button"
              tabIndex="0"
            >
              <MaterialIcon icon="star" />
            </div>
            <div
              className="rate-star"
              onMouseEnter={() => this.starEnterhandler(60)}
              onClick={this.starClickHandler}
              onKeyPress={() => {}}
              role="button"
              tabIndex="0"
            >
              <MaterialIcon icon="star" />
            </div>
            <div
              className="rate-star"
              onMouseEnter={() => this.starEnterhandler(80)}
              onClick={this.starClickHandler}
              onKeyPress={() => {}}
              role="button"
              tabIndex="0"
            >
              <MaterialIcon icon="star" />
            </div>
            <div
              className="rate-star"
              onMouseEnter={() => this.starEnterhandler(100)}
              onClick={this.starClickHandler}
              onKeyPress={() => {}}
              role="button"
              tabIndex="0"
            >
              <MaterialIcon icon="star" />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

RateButton.propTypes = {
  rate: PropTypes.oneOfType([PropTypes.bool, PropTypes.object]),
  rateHandler: PropTypes.func.isRequired,
};

RateButton.defaultProps = {
  rate: false,
};

export default RateButton;
