import React from 'react';
import { Link, NavLink } from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import './styles.css';

function AppHeader(props) {
  return (
    <header className="app-header">
      <div className="container">
        <div className="header-inner">
          <div className="logo-wrap">
            <Link to="/">
              <h1 className="brand">React Redux Movies App</h1>
            </Link>
          </div>
          <nav className="app-header-menu">
            <ul>
              <li>
                <NavLink exact to="/" activeClassName="active">
                  Home
                </NavLink>
              </li>
              <li>
                <NavLink to="/movies" activeClassName="active">
                  Movies
                </NavLink>
              </li>
              <li>
                <NavLink to="/tvs" activeClassName="active">
                  TV Shows
                </NavLink>
              </li>
              {props.isLoggedIn ? (
                <React.Fragment>
                  <li>
                    <NavLink to="/user" activeClassName="active">
                      User
                    </NavLink>
                  </li>
                  <li>
                    <NavLink to="/logout" activeClassName="active">
                      Logout
                    </NavLink>
                  </li>
                </React.Fragment>
              ) : (
                <li>
                  <NavLink to="/login" activeClassName="active">
                    Login
                  </NavLink>
                </li>
              )}
            </ul>
          </nav>
        </div>
      </div>
    </header>
  );
}

AppHeader.propTypes = {
  isLoggedIn: PropTypes.bool,
};

AppHeader.defaultProps = {
  isLoggedIn: false,
};

const mapStateToProps = state => ({
  isLoggedIn: state.auth.isLoggedIn,
  pathname: state.router.location.pathname,
  search: state.router.location.search,
  hash: state.router.location.hash,
});

export default connect(mapStateToProps, null)(AppHeader);
