import React from 'react';
import PropTypes from 'prop-types';

import TMDB from '../../helpers/TMDB';

import './styles.css';

function ImagesGallery(props) {
  const { items } = props;
  if (items.length === 0) {
    return null;
  }
  const itemsList = items.map((item, index) => (
    <div key={index} className="gallery-item">
      <img src={TMDB.common.getImage({ size: 300, file: item.file_path })} alt="test" />
    </div>
  ));
  return (
    <div className="images-gallery-wrap">
      <h3 className="gallery-title">Gallery</h3>
      <div className="images-gallery-grid">{itemsList}</div>
    </div>
  );
}

ImagesGallery.propTypes = {
  items: PropTypes.arrayOf(PropTypes.object).isRequired,
};

export default ImagesGallery;
