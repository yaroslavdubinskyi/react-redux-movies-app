import React from 'react';

import spiner from './oval.svg';
import './styles.css';

function Loader() {
  return (
    <div className="loader">
      <img src={spiner} alt="loader" />
    </div>
  );
}

export default Loader;
