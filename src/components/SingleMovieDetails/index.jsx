import React from 'react';
import MaterialIcon from 'react-google-material-icons';
import PropTypes from 'prop-types';

import TMDB from '../../helpers/TMDB';

import RatingBadge from '../RatingBadge';
import FavoriteButton from '../FavoriteButton';
import RateButton from '../RateButton';

import './styles.css';

function SingleMovieDetails({
  item,
  accountStates,
  favoriteMovieHandler,
  rateHandler,
  isLoggedIn,
}) {
  const { title, overview, tagline, budget, runtime } = item;

  const releaseDate = item.release_date;
  const rating = item.vote_average;
  const vouteCount = item.vote_count;
  const posterSrc = TMDB.common.getImage({ size: 342, file: item.poster_path });
  const backdropSrc = TMDB.common.getImage({
    size: 780,
    file: item.backdrop_path,
  });
  return (
    <section className="single-movie-details">
      <img src={backdropSrc} alt="Backdrop" className="backdrop-bg" />
      <div className="container">
        <div className="cols-wrap">
          <div className="poster-col">
            <img src={posterSrc} alt={title} className="poster" />
          </div>
          <div className="desc-col">
            <div className="desc-wrap">
              <h2>{title}</h2>
              <p className="tagline">{tagline}</p>
              <ul className="short-info">
                <li>
                  <MaterialIcon icon="date_range" /> <span>{releaseDate}</span>
                </li>
                <li>
                  <MaterialIcon icon="attach_money" />
                  <span>{budget}</span>
                </li>
                <li>
                  <MaterialIcon icon="access_time" /> <span>{runtime} m.</span>
                </li>
              </ul>
              <p>{overview}</p>
            </div>
          </div>
          <div className="rating-col">
            <RatingBadge rating={rating} count={vouteCount} />
            {isLoggedIn && (
              <React.Fragment>
                <FavoriteButton
                  favorite={accountStates.favorite}
                  favoriteHandler={favoriteMovieHandler}
                />
                <RateButton rate={accountStates.rated} rateHandler={rateHandler} />
              </React.Fragment>
            )}
          </div>
        </div>
      </div>
    </section>
  );
}

SingleMovieDetails.propTypes = {
  item: PropTypes.shape({
    title: PropTypes.string,
    overview: PropTypes.string,
    tagline: PropTypes.string,
    release_date: PropTypes.string,
    budget: PropTypes.number,
    vote_average: PropTypes.number,
    runtime: PropTypes.number,
    poster_path: PropTypes.string,
    backdrop_path: PropTypes.string,
    vote_count: PropTypes.number,
    rating: PropTypes.number,
  }).isRequired,
  accountStates: PropTypes.object,
  favoriteMovieHandler: PropTypes.func.isRequired,
  rateHandler: PropTypes.func.isRequired,
  isLoggedIn: PropTypes.bool,
};

SingleMovieDetails.defaultProps = {
  isLoggedIn: false,
};

export default SingleMovieDetails;
