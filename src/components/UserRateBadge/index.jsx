import React from 'react';
import PropTypes from 'prop-types';
import MaterialIcon from 'react-google-material-icons';

import './styles.css';

function UserRateBadge({ rate }) {
  return (
    <div className="user-rate-badge-wrap">
      <MaterialIcon icon="star" />
      <span>{rate}</span>
    </div>
  );
}

UserRateBadge.propTypes = {
  rate: PropTypes.number.isRequired,
};

export default UserRateBadge;
