import React from 'react';
import PropTypes from 'prop-types';
import MaterialIcon from 'react-google-material-icons';

import './styles.css';

function FavoriteButton(props) {
  const isFavorite = props.favorite || false;

  return (
    <div
      className="favorite-button-wrap"
      onClick={props.favoriteHandler}
      onKeyPress={() => {}}
      role="button"
      tabIndex="0"
    >
      {isFavorite ? <MaterialIcon icon="favorite" /> : <MaterialIcon icon="favorite_border" />}
    </div>
  );
}

FavoriteButton.propTypes = {
  favorite: PropTypes.bool,
  favoriteHandler: PropTypes.func.isRequired,
};

FavoriteButton.defaultProps = {
  favorite: false,
};

export default FavoriteButton;
