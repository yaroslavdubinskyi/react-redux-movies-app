import React from 'react';
import MaterialIcon from 'react-google-material-icons';
import { shallow } from 'enzyme';

import FavoriteButton from './index';

describe('<FavoriteButton />', () => {
  it('should render an <div> tag', () => {
    const onClickSpy = jest.fn();
    const renderedComponent = shallow(<FavoriteButton favoriteHandler={onClickSpy} />);
    expect(renderedComponent.type()).toEqual('div');
  });

  it('should have a className attribute', () => {
    const onClickSpy = jest.fn();
    const renderedComponent = shallow(<FavoriteButton favoriteHandler={onClickSpy} />);
    expect(renderedComponent.find('div').prop('className')).toBeDefined();
  });

  it('should handle click events', () => {
    const onClickSpy = jest.fn();
    const renderedComponent = shallow(<FavoriteButton favoriteHandler={onClickSpy} />);
    renderedComponent.find('div').simulate('click');
    expect(onClickSpy).toHaveBeenCalled();
  });

  it('should has filled star icon when isFavorite is true', () => {
    const onClickSpy = jest.fn();
    const renderedComponent = shallow(<FavoriteButton favoriteHandler={onClickSpy} favorite />);
    expect(renderedComponent.contains(<MaterialIcon icon="favorite" />)).toEqual(true);
  });

  it('should has bordered star icon when isFavorite is false', () => {
    const onClickSpy = jest.fn();
    const renderedComponent = shallow(<FavoriteButton
      favoriteHandler={onClickSpy}
      favorite={false}
    />);
    expect(renderedComponent.contains(<MaterialIcon icon="favorite_border" />)).toEqual(true);
  });
});
