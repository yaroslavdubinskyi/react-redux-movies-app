import React from 'react';
import PropTypes from 'prop-types';

import './styles.css';

function RatingBadge({ rating, count }) {
  const ratingHeight = rating * 10;
  let ratingColor = '#ff0000';
  if (rating <= 4) {
    ratingColor = '#ff0000';
  } else {
    ratingColor = rating <= 7 ? '#feff14' : '#0fff16';
  }
  const badgeStyle = {
    height: `${ratingHeight}%`,
    backgroundColor: ratingColor,
  };
  return (
    <div className="item__rating__wrap">
      <div className="bg-outer" style={badgeStyle} />
      <div className="inner">
        <span className="rating">{rating}</span>
        <span className="voute-count">{count}</span>
      </div>
    </div>
  );
}

RatingBadge.propTypes = {
  rating: PropTypes.number,
  count: PropTypes.number,
};

RatingBadge.defaultProps = {
  rating: 0,
  count: 0,
};

export default RatingBadge;
