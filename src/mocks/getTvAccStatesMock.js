const getTvAccStatesMock = {
  id: 1396,
  favorite: true,
  rated: {
    value: 5,
  },
  watchlist: false,
};

export default getTvAccStatesMock;
