const getMovieAccStatesMock = {
  id: 284053,
  favorite: false,
  rated: false,
  watchlist: false,
};

export default getMovieAccStatesMock;
