const getMovieSimilarMock = {
  page: 1,
  results: [
    {
      adult: false,
      backdrop_path: '/pkX4ytzAXswpMhea0JKxgA5Vmqo.jpg',
      genre_ids: [12, 14, 28, 878],
      id: 1452,
      original_language: 'en',
      original_title: 'Superman Returns',
      overview:
        'Superman returns to discover his 5-year absence has allowed Lex Luthor to walk free, and that those he was closest to felt abandoned and have moved on. Luthor plots his ultimate revenge that could see millions killed and change the face of the planet forever, as well as ridding himself of the Man of Steel.',
      release_date: '2006-06-28',
      poster_path: '/e3aLTaD5ppxo3en0GAGceekEPAe.jpg',
      popularity: 25.676105,
      title: 'Superman Returns',
      video: false,
      vote_average: 5.4,
      vote_count: 1735,
    },
  ],
  total_pages: 15,
  total_results: 288,
};

export default getMovieSimilarMock;
