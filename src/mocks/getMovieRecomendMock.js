const getMovieRecomendMock = {
  page: 1,
  results: [
    {
      adult: false,
      backdrop_path: '/o5T8rZxoWSBMYwjsUFUqTt6uMQB.jpg',
      genre_ids: [28, 12, 14, 878],
      id: 141052,
      original_language: 'en',
      original_title: 'Justice League',
      overview:
        "Fuelled by his restored faith in humanity and inspired by Superman's selfless act, Bruce Wayne and Diana Prince assemble a team of metahumans consisting of Barry Allen, Arthur Curry and Victor Stone to face the catastrophic threat of Steppenwolf and the Parademons who are on the hunt for three Mother Boxes on Earth.",
      poster_path: '/eifGNCSDuxJeS1loAXil5bIGgvC.jpg',
      release_date: '2017-11-15',
      title: 'Justice League',
      video: false,
      vote_average: 6.4,
      vote_count: 3928,
    },
  ],
  total_pages: 1,
  total_results: 20,
};

export default getMovieRecomendMock;
