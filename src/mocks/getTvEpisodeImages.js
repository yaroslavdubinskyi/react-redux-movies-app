const getTvEpisodeImages = {
  id: 62100,
  stills: [
    {
      aspect_ratio: 1.777777777777778,
      file_path: '/j92CoMwArTeQwnGVl12SJaKlI0J.jpg',
      height: 1080,
      iso_639_1: null,
      vote_average: 5.3125,
      vote_count: 1,
      width: 1920,
    },
    {
      aspect_ratio: 1.777777777777778,
      file_path: '/fmM4rKDi7P7Npobgq5oH1Nszvv3.jpg',
      height: 1080,
      iso_639_1: null,
      vote_average: 5.238095238095238,
      vote_count: 2,
      width: 1920,
    },
    {
      aspect_ratio: 1.79372197309417,
      file_path: '/4E9DKjGO5UQVKCmyUEwfiMpymDR.jpg',
      height: 223,
      iso_639_1: null,
      vote_average: 5.163690476190476,
      vote_count: 1,
      width: 400,
    },
  ],
};

export default getTvEpisodeImages;
