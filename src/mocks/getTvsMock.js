const getTvsMock = {
  page: 1,
  total_results: 1461,
  total_pages: 74,
  results: [
    {
      original_name: 'The Big Bang Theory',
      genre_ids: [35],
      name: 'The Big Bang Theory',
      popularity: 452.516922,
      origin_country: ['US'],
      vote_count: 2790,
      first_air_date: '2007-09-24',
      backdrop_path: '/nGsNruW3W27V6r4gkyc3iiEGsKR.jpg',
      original_language: 'en',
      id: 1418,
      vote_average: 6.8,
      overview:
        "The Big Bang Theory is centered on five characters living in Pasadena, California: roommates Leonard Hofstadter and Sheldon Cooper; Penny, a waitress and aspiring actress who lives across the hall; and Leonard and Sheldon's equally geeky and socially awkward friends and co-workers, mechanical engineer Howard Wolowitz and astrophysicist Raj Koothrappali. The geekiness and intellect of the four guys is contrasted for comic effect with Penny's social skills and common sense.",
      poster_path: '/ooBGRQBdbGzBxAVfExiO8r7kloA.jpg',
    },
  ],
};

export default getTvsMock;
