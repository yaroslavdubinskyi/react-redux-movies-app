const getMoviesMock = {
  page: 1,
  total_results: 16926,
  total_pages: 847,
  results: [
    {
      vote_count: 1196,
      id: 337167,
      video: false,
      vote_average: 6.1,
      title: 'Fifty Shades Freed',
      popularity: 457.27461,
      poster_path: '/3kcEGnYBHDeqmdYf8ZRbKdfmlUy.jpg',
      original_language: 'en',
      original_title: 'Fifty Shades Freed',
      genre_ids: [18, 10749],
      backdrop_path: '/9ywA15OAiwjSTvg3cBs9B7kOCBF.jpg',
      adult: false,
      overview:
        'Believing they have left behind shadowy figures from their past, newlyweds Christian and Ana fully embrace an inextricable connection and shared life of luxury. But just as she steps into her role as Mrs. Grey and he relaxes into an unfamiliar stability, new threats could jeopardize their happy ending before it even begins.',
      release_date: '2018-02-07',
    },
  ],
};

export default getMoviesMock;
