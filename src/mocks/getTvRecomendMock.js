const getTvRecomendMock = {
  page: 1,
  results: [
    {
      backdrop_path: '/5m05BIoMHgTd4zvJ5OBh7gZFGWV.jpg',
      first_air_date: '2006-10-01',
      genre_ids: [80, 18, 9648],
      id: 1405,
      name: 'Dexter',
      origin_country: ['US'],
      original_language: 'en',
      original_name: 'Dexter',
      overview:
        "Dexter is an American television drama series. The series centers on Dexter Morgan, a blood spatter pattern analyst for 'Miami Metro Police Department' who also leads a secret life as a serial killer, hunting down criminals who have slipped through the cracks of justice.",
      poster_path: '/ydmfheI5cJ4NrgcupDEwk8I8y5q.jpg',
      vote_average: 8,
      vote_count: 955,
      networks: [
        {
          id: 67,
          logo: {
            path: '/Allse9kbjiP6ExaQrnSpIhkurEi.png',
            aspect_ratio: 2.42080378250591,
          },
          name: 'Showtime',
          origin_country: 'US',
        },
      ],
    },
  ],
  total_pages: 1,
  total_results: 20,
};

export default getTvRecomendMock;
