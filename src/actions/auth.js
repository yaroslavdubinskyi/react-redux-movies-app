const API_KEY = '24887d21d7f4a96154895b95c9723021';
const BASE_URL = 'https://api.themoviedb.org/3';

export const GET_REQ_TOKEN_REQUEST = 'GET_REQ_TOKEN_REQUEST';
export const GET_REQ_TOKEN_SUCCESS = 'GET_REQ_TOKEN_SUCCESS';
export const GET_REQ_TOKEN_FAILURE = 'GET_REQ_TOKEN_FAILURE';

export const generateToken = () => {
  const path = '/authentication/token/new';
  const query = `api_key=${API_KEY}`;
  return dispatch => {
    dispatch({
      type: GET_REQ_TOKEN_REQUEST,
    });
    fetch(`${BASE_URL}${path}?${query}`)
      .then(response => response.json())
      .then(data =>
        dispatch({
          type: GET_REQ_TOKEN_SUCCESS,
          payload: data,
        }),
      )
      .catch(error =>
        dispatch({
          type: GET_REQ_TOKEN_FAILURE,
          payload: error,
          error: true,
        }),
      );
  };
};

export const GET_SESSION_ID_REQUEST = 'GET_SESSION_ID_REQUEST';
export const GET_SESSION_ID_SUCCESS = 'GET_SESSION_ID_SUCCESS';
export const GET_SESSION_ID_FAILURE = 'GET_SESSION_ID_FAILURE';

export const generateSession = req_token => {
  const path = '/authentication/session/new';
  const query = `api_key=${API_KEY}&request_token=${req_token}`;
  return dispatch => {
    dispatch({
      type: GET_SESSION_ID_REQUEST,
    });
    fetch(`${BASE_URL}${path}?${query}`)
      .then(response => response.json())
      .then(data =>
        dispatch({
          type: GET_SESSION_ID_SUCCESS,
          payload: data,
        }),
      )
      .catch(error =>
        dispatch({
          type: GET_SESSION_ID_FAILURE,
          payload: error,
          error: true,
        }),
      );
  };
};

export const LOGOUT_USER = 'LOGOUT_USER';

export const logoutUser = () => ({
  type: LOGOUT_USER,
});
