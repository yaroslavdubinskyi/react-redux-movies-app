import TMDB from "../helpers/TMDB";

export const GET_MOVIES_REQUEST = "GET_MOVIES_REQUEST";
export const GET_MOVIES_FAILURE = "GET_MOVIES_FAILURE";
export const GET_MOVIES_SUCCESS = "GET_MOVIES_SUCCESS";

export const loadMovies = options => dispatch => {
  dispatch({
    type: GET_MOVIES_REQUEST
  });
  TMDB.discover
    .getMovies(options)
    .then(response => response.json())
    .then(data =>
      dispatch({
        type: GET_MOVIES_SUCCESS,
        payload: data
      })
    )
    .catch(error =>
      dispatch({
        type: GET_MOVIES_FAILURE,
        payload: error,
        error: true
      })
    );
};

export const GET_MOVIE_DETAILS_REQUEST = "GET_MOVIE_DETAILS_REQUEST";
export const GET_MOVIE_DETAILS_FAILURE = "GET_MOVIE_DETAILS_FAILURE";
export const GET_MOVIE_DETAILS_SUCCESS = "GET_MOVIE_DETAILS_SUCCESS";

export const getMovieDetails = movie_id => dispatch => {
  dispatch({
    type: GET_MOVIE_DETAILS_REQUEST
  });
  TMDB.movie
    .getDetails({ id: movie_id })
    .then(response => response.json())
    .then(data =>
      dispatch({
        type: GET_MOVIE_DETAILS_SUCCESS,
        payload: data
      })
    )
    .catch(error =>
      dispatch({
        type: GET_MOVIE_DETAILS_FAILURE,
        payload: error,
        error: true
      })
    );
};

export const GET_MOVIE_ACC_STATES_REQUEST = "GET_MOVIE_ACC_STATES_REQUEST";
export const GET_MOVIE_ACC_STATES_FAILURE = "GET_MOVIE_ACC_STATES_FAILURE";
export const GET_MOVIE_ACC_STATES_SUCCESS = "GET_MOVIE_ACC_STATES_SUCCESS";

export const getMovieAccountStates = options => dispatch => {
  dispatch({
    type: GET_MOVIE_ACC_STATES_REQUEST
  });
  TMDB.movie
    .getAccountStates(options)
    .then(response => response.json())
    .then(data =>
      dispatch({
        type: GET_MOVIE_ACC_STATES_SUCCESS,
        payload: data
      })
    )
    .catch(error =>
      dispatch({
        type: GET_MOVIE_ACC_STATES_FAILURE,
        payload: error,
        error: true
      })
    );
};

export const GET_MOVIE_RECOMEND_REQUEST = "GET_MOVIE_RECOMEND_REQUEST";
export const GET_MOVIE_RECOMEND_FAILURE = "GET_MOVIE_RECOMEND_FAILURE";
export const GET_MOVIE_RECOMEND_SUCCESS = "GET_MOVIE_RECOMEND_SUCCESS";

export const getMovieRecomendations = movie_id => dispatch => {
  dispatch({
    type: GET_MOVIE_RECOMEND_REQUEST
  });
  TMDB.movie
    .getRecomendations({ id: movie_id })
    .then(response => response.json())
    .then(data =>
      dispatch({
        type: GET_MOVIE_RECOMEND_SUCCESS,
        payload: data
      })
    )
    .catch(error =>
      dispatch({
        type: GET_MOVIE_RECOMEND_FAILURE,
        payload: error,
        error: true
      })
    );
};

export const GET_MOVIE_SIMILAR_REQUEST = "GET_MOVIE_SIMILAR_REQUEST";
export const GET_MOVIE_SIMILAR_FAILURE = "GET_MOVIE_SIMILAR_FAILURE";
export const GET_MOVIE_SIMILAR_SUCCESS = "GET_MOVIE_SIMILAR_SUCCESS";

export const getMovieSimilars = movie_id => dispatch => {
  dispatch({
    type: GET_MOVIE_SIMILAR_REQUEST
  });
  TMDB.movie
    .getSimilar({ id: movie_id })
    .then(response => response.json())
    .then(data =>
      dispatch({
        type: GET_MOVIE_SIMILAR_SUCCESS,
        payload: data
      })
    )
    .catch(error =>
      dispatch({
        type: GET_MOVIE_SIMILAR_FAILURE,
        payload: error,
        error: true
      })
    );
};

export const RATE_MOVIE_REQUEST = "RATE_MOVIE_REQUEST";
export const RATE_MOVIE_FAILURE = "RATE_MOVIE_FAILURE";
export const RATE_MOVIE_SUCCESS = "RATE_MOVIE_SUCCESS";

export const rateMovie = options => dispatch => {
  dispatch({
    type: RATE_MOVIE_REQUEST
  });
  return fetch(
    `${TMDB.common.base_uri}movie/${options.id}/rating?api_key=${
      TMDB.common.api_key
    }&session_id=${options.session_id}`,
    {
      body: JSON.stringify(options.data),
      method: "POST",
      headers: {
        "content-type": "application/json;charset=utf-8"
      }
    }
  )
    .then(response => response.json())
    .then(data =>
      dispatch({
        type: RATE_MOVIE_SUCCESS,
        payload: data
      })
    )
    .catch(error =>
      dispatch({
        type: RATE_MOVIE_FAILURE,
        payload: error,
        error: true
      })
    );
};
