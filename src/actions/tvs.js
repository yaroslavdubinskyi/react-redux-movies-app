import TMDB from "../helpers/TMDB";

export const GET_TVS_REQUEST = "GET_TVS_REQUEST";
export const GET_TVS_FAILURE = "GET_TVS_FAILURE";
export const GET_TVS_SUCCESS = "GET_TVS_SUCCESS";

export const loadTvShows = options => dispatch => {
  dispatch({
    type: GET_TVS_REQUEST
  });
  TMDB.discover
    .getTvShows(options)
    .then(response => response.json())
    .then(data =>
      dispatch({
        type: GET_TVS_SUCCESS,
        payload: data
      })
    )
    .catch(error =>
      dispatch({
        type: GET_TVS_FAILURE,
        payload: error,
        error: true
      })
    );
};

export const GET_TV_DETAILS_REQUEST = "GET_TV_DETAILS_REQUEST";
export const GET_TV_DETAILS_FAILURE = "GET_TV_DETAILS_FAILURE";
export const GET_TV_DETAILS_SUCCESS = "GET_TV_DETAILS_SUCCESS";

export const getTVDetails = tv_id => dispatch => {
  dispatch({
    type: GET_TV_DETAILS_REQUEST
  });
  TMDB.tv
    .getDetails({ id: tv_id })
    .then(response => response.json())
    .then(data =>
      dispatch({
        type: GET_TV_DETAILS_SUCCESS,
        payload: data
      })
    )
    .catch(error =>
      dispatch({
        type: GET_TV_DETAILS_FAILURE,
        payload: error,
        error: true
      })
    );
};

export const GET_TV_ACC_STATES_REQUEST = "GET_TV_ACC_STATES_REQUEST";
export const GET_TV_ACC_STATES_FAILURE = "GET_TV_ACC_STATES_FAILURE";
export const GET_TV_ACC_STATES_SUCCESS = "GET_TV_ACC_STATES_SUCCESS";

export const getTVAccountStates = options => dispatch => {
  dispatch({
    type: GET_TV_ACC_STATES_REQUEST
  });
  return TMDB.tv
    .getAccountStates(options)
    .then(response => response.json())
    .then(data =>
      dispatch({
        type: GET_TV_ACC_STATES_SUCCESS,
        payload: data
      })
    )
    .catch(error =>
      dispatch({
        type: GET_TV_ACC_STATES_FAILURE,
        payload: error,
        error: true
      })
    );
};

export const GET_TV_RECOMEND_REQUEST = "GET_TV_RECOMEND_REQUEST";
export const GET_TV_RECOMEND_FAILURE = "GET_TV_RECOMEND_FAILURE";
export const GET_TV_RECOMEND_SUCCESS = "GET_TV_RECOMEND_SUCCESS";

export const getTVRecomendations = tv_id => dispatch => {
  dispatch({
    type: GET_TV_RECOMEND_REQUEST
  });
  TMDB.tv
    .getRecomendations({ id: tv_id })
    .then(response => response.json())
    .then(data =>
      dispatch({
        type: GET_TV_RECOMEND_SUCCESS,
        payload: data
      })
    )
    .catch(error =>
      dispatch({
        type: GET_TV_RECOMEND_FAILURE,
        payload: error,
        error: true
      })
    );
};

export const GET_TV_SIMILAR_REQUEST = "GET_TV_SIMILAR_REQUEST";
export const GET_TV_SIMILAR_FAILURE = "GET_TV_SIMILAR_FAILURE";
export const GET_TV_SIMILAR_SUCCESS = "GET_TV_SIMILAR_SUCCESS";

export const getTVSimilars = tv_id => dispatch => {
  dispatch({
    type: GET_TV_SIMILAR_REQUEST
  });
  TMDB.tv
    .getSimilar({ id: tv_id })
    .then(response => response.json())
    .then(data =>
      dispatch({
        type: GET_TV_SIMILAR_SUCCESS,
        payload: data
      })
    )
    .catch(error =>
      dispatch({
        type: GET_TV_SIMILAR_FAILURE,
        payload: error,
        error: true
      })
    );
};

export const GET_TV_SEASON_REQUEST = "GET_TV_SEASON_REQUEST";
export const GET_TV_SEASON_FAILURE = "GET_TV_SEASON_FAILURE";
export const GET_TV_SEASON_SUCCESS = "GET_TV_SEASON_SUCCESS";

export const getTVSeason = (tv_id, season_number) => dispatch => {
  dispatch({
    type: GET_TV_SEASON_REQUEST
  });
  return TMDB.tvSeason
    .getDetails({ id: tv_id, season_number: season_number })
    .then(response => response.json())
    .then(data =>
      dispatch({
        type: GET_TV_SEASON_SUCCESS,
        payload: data
      })
    )
    .catch(error => {
      console.log(error);
      dispatch({
        type: GET_TV_SEASON_FAILURE,
        payload: error,
        error: true
      });
    });
};

export const GET_TV_EPISODE_REQUEST = "GET_TV_EPISODE_REQUEST";
export const GET_TV_EPISODE_FAILURE = "GET_TV_EPISODE_FAILURE";
export const GET_TV_EPISODE_SUCCESS = "GET_TV_EPISODE_SUCCESS";

export const getTVEpisode = (
  tv_id,
  season_number,
  episode_number
) => dispatch => {
  dispatch({
    type: GET_TV_EPISODE_REQUEST
  });
  return TMDB.tvEpisode
    .getDetails({
      id: tv_id,
      season_number: season_number,
      episode_number: episode_number
    })
    .then(response => response.json())
    .then(data =>
      dispatch({
        type: GET_TV_EPISODE_SUCCESS,
        payload: data
      })
    )
    .catch(error => {
      console.log(error);
      dispatch({
        type: GET_TV_EPISODE_FAILURE,
        payload: error,
        error: true
      });
    });
};

export const GET_TV_EPISODE_IMAGES_REQUEST = "GET_TV_EPISODE_IMAGES_REQUEST";
export const GET_TV_EPISODE_IMAGES_FAILURE = "GET_TV_EPISODE_IMAGES_FAILURE";
export const GET_TV_EPISODE_IMAGES_SUCCESS = "GET_TV_EPISODE_IMAGES_SUCCESS";

export const getTVEpisodeImages = (
  tv_id,
  season_number,
  episode_number
) => dispatch => {
  dispatch({
    type: GET_TV_EPISODE_IMAGES_REQUEST
  });
  return TMDB.tvEpisode
    .getImages({
      id: tv_id,
      season_number: season_number,
      episode_number: episode_number
    })
    .then(response => response.json())
    .then(data =>
      dispatch({
        type: GET_TV_EPISODE_IMAGES_SUCCESS,
        payload: data
      })
    )
    .catch(error => {
      console.log(error);
      dispatch({
        type: GET_TV_EPISODE_IMAGES_FAILURE,
        payload: error,
        error: true
      });
    });
};

export const RATE_TV_REQUEST = "RATE_TV_REQUEST";
export const RATE_TV_FAILURE = "RATE_TV_FAILURE";
export const RATE_TV_SUCCESS = "RATE_TV_SUCCESS";

export const rateTV = options => dispatch => {
  dispatch({
    type: RATE_TV_REQUEST
  });
  return fetch(
    `${TMDB.common.base_uri}tv/${options.id}/rating?api_key=${
      TMDB.common.api_key
    }&session_id=${options.session_id}`,
    {
      body: JSON.stringify(options.data),
      method: "POST",
      headers: {
        "content-type": "application/json;charset=utf-8"
      }
    }
  )
    .then(response => response.json())
    .then(data =>
      dispatch({
        type: RATE_TV_SUCCESS,
        payload: data
      })
    )
    .catch(error =>
      dispatch({
        type: RATE_TV_FAILURE,
        payload: error,
        error: true
      })
    );
};
