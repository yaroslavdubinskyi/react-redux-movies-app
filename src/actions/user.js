import TMDB from "../helpers/TMDB";

export const GET_USER_DETAILS_REQUEST = "GET_USER_DETAILS_REQUEST";
export const GET_USER_DETAILS_FAILURE = "GET_USER_DETAILS_FAILURE";
export const GET_USER_DETAILS_SUCCESS = "GET_USER_DETAILS_SUCCESS";

export const getUserDetails = options => dispatch => {
  dispatch({
    type: GET_USER_DETAILS_REQUEST
  });
  return TMDB.account
    .getDetails(options)
    .then(response => response.json())
    .then(data =>
      dispatch({
        type: GET_USER_DETAILS_SUCCESS,
        payload: data
      })
    )
    .catch(error =>
      dispatch({
        type: GET_USER_DETAILS_FAILURE,
        payload: error,
        error: true
      })
    );
};

export const GET_USER_RATED_MOVIES_REQUEST = "GET_USER_RATED_MOVIES_REQUEST";
export const GET_USER_RATED_MOVIES_FAILURE = "GET_USER_RATED_MOVIES_FAILURE";
export const GET_USER_RATED_MOVIES_SUCCESS = "GET_USER_RATED_MOVIES_SUCCESS";

export const getUserRatedMovies = options => dispatch => {
  dispatch({
    type: GET_USER_RATED_MOVIES_REQUEST
  });
  TMDB.account
    .getRatedMovies(options)
    .then(response => response.json())
    .then(data =>
      dispatch({
        type: GET_USER_RATED_MOVIES_SUCCESS,
        payload: data
      })
    )
    .catch(error =>
      dispatch({
        type: GET_USER_RATED_MOVIES_FAILURE,
        payload: error,
        error: true
      })
    );
};

export const GET_USER_RATED_TV_REQUEST = "GET_USER_RATED_TV_REQUEST";
export const GET_USER_RATED_TV_FAILURE = "GET_USER_RATED_TV_FAILURE";
export const GET_USER_RATED_TV_SUCCESS = "GET_USER_RATED_TV_SUCCESS";

export const getUserRatedTvShows = options => dispatch => {
  dispatch({
    type: GET_USER_RATED_TV_REQUEST
  });
  TMDB.account
    .getRatedTvShows(options)
    .then(response => response.json())
    .then(data =>
      dispatch({
        type: GET_USER_RATED_TV_SUCCESS,
        payload: data
      })
    )
    .catch(error =>
      dispatch({
        type: GET_USER_RATED_TV_FAILURE,
        payload: error,
        error: true
      })
    );
};

export const GET_USER_FAV_MOVIES_REQUEST = "GET_USER_FAV_MOVIES_REQUEST";
export const GET_USER_FAV_MOVIES_FAILURE = "GET_USER_FAV_MOVIES_FAILURE";
export const GET_USER_FAV_MOVIES_SUCCESS = "GET_USER_FAV_MOVIES_SUCCESS";

export const getUserFavoriteMovies = options => dispatch => {
  dispatch({
    type: GET_USER_FAV_MOVIES_REQUEST
  });
  TMDB.account
    .getFavoriteMovies(options)
    .then(response => response.json())
    .then(data =>
      dispatch({
        type: GET_USER_FAV_MOVIES_SUCCESS,
        payload: data
      })
    )
    .catch(error =>
      dispatch({
        type: GET_USER_FAV_MOVIES_FAILURE,
        payload: error,
        error: true
      })
    );
};

export const GET_USER_FAV_TV_REQUEST = "GET_USER_FAV_TV_REQUEST";
export const GET_USER_FAV_TV_FAILURE = "GET_USER_FAV_TV_FAILURE";
export const GET_USER_FAV_TV_SUCCESS = "GET_USER_FAV_TV_SUCCESS";

export const getUserFavoriteTvShows = options => dispatch => {
  dispatch({
    type: GET_USER_FAV_TV_REQUEST
  });
  TMDB.account
    .getFavoriteTvShows(options)
    .then(response => response.json())
    .then(data =>
      dispatch({
        type: GET_USER_FAV_TV_SUCCESS,
        payload: data
      })
    )
    .catch(error =>
      dispatch({
        type: GET_USER_FAV_TV_FAILURE,
        payload: error,
        error: true
      })
    );
};

export const MARK_AS_FAV_REQUEST = "MARK_AS_FAV_REQUEST";
export const MARK_AS_FAV_FAILURE = "MARK_AS_FAV_FAILURE";
export const MARK_AS_FAV_SUCCESS = "MARK_AS_FAV_SUCCESS";

export const markAsFavorite = options => dispatch => {
  dispatch({
    type: MARK_AS_FAV_REQUEST
  });
  return fetch(
    `${TMDB.common.base_uri}account/${options.id}/favorite?api_key=${
      TMDB.common.api_key
    }&session_id=${options.session_id}`,
    {
      body: JSON.stringify(options.data),
      method: "POST",
      headers: {
        "content-type": "application/json;charset=utf-8"
      }
    }
  )
    .then(response => response.json())
    .then(data =>
      dispatch({
        type: MARK_AS_FAV_SUCCESS,
        payload: data
      })
    )
    .catch(error =>
      dispatch({
        type: MARK_AS_FAV_FAILURE,
        payload: error,
        error: true
      })
    );
};
