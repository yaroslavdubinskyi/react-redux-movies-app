import { applyMiddleware, compose, createStore } from 'redux';
import { createBrowserHistory } from 'history';
import { routerMiddleware, connectRouter } from 'connected-react-router';
import thunk from 'redux-thunk';
import { loadState, saveState } from './localStorage';
import rootReducer from '../reducers';

export const history = createBrowserHistory();

/*
eslint no-underscore-dangle: ["error", { "allow": ["__REDUX_DEVTOOLS_EXTENSION_COMPOSE__", ] }]
 */
const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const persistedState = loadState();
const store = createStore(
  connectRouter(history)(rootReducer),
  persistedState,
  composeEnhancer(applyMiddleware(routerMiddleware(history), thunk)),
);

store.subscribe(() => {
  saveState({
    auth: store.getState().auth,
    user: store.getState().user,
  });
});

export default store;
