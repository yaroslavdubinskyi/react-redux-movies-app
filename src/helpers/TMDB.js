const TMDB = {};

TMDB.common = {
  api_key: '24887d21d7f4a96154895b95c9723021',
  base_uri: 'https://api.themoviedb.org/3/',
  images_uri: 'http://image.tmdb.org/t/p/',

  generateQuery: options => {
    const myOptions = options || {};
    let query = `?api_key=${TMDB.common.api_key}`;
    let option;

    if (Object.keys(myOptions).length > 0) {
      for (option in myOptions) {
        if (
          myOptions.hasOwnProperty(option) &&
          option !== 'id' &&
          option !== 'body' &&
          option !== 'season_number'
        ) {
          query = query + '&' + option + '=' + myOptions[option];
        }
      }
    }
    return query;
  },
  getImage: (options) => {
    return options.file === null
      ? '/icon-img-placeholder.svg'
      : `${TMDB.common.images_uri}w${options.size}${options.file}`;
  },
};

TMDB.discover = {
  getMovies: (
    options = {
      page: 1,
      sort_by: 'popularity.desc',
    },
  ) =>
    fetch(
      `${TMDB.common.base_uri}discover/movie${TMDB.common.generateQuery(
        options,
      )}&vote_count.gte=25`,
    ),

  getTvShows: (
    options = {
      page: 1,
      sort_by: 'popularity.desc',
    },
  ) =>
    fetch(
      `${TMDB.common.base_uri}discover/tv${TMDB.common.generateQuery(options)}&vote_count.gte=25`,
    ),
};

TMDB.account = {
  getDetails: options =>
    fetch(`${TMDB.common.base_uri}account${TMDB.common.generateQuery(options)}`),
  getFavoriteMovies: options =>
    fetch(
      `${TMDB.common.base_uri}account/${options.id}/favorite/movies${TMDB.common.generateQuery(
        options,
      )}`,
    ),
  getFavoriteTvShows: options =>
    fetch(
      `${TMDB.common.base_uri}account/${options.id}/favorite/tv${TMDB.common.generateQuery(
        options,
      )}`,
    ),
  getRatedMovies: options =>
    fetch(
      `${TMDB.common.base_uri}account/${options.id}/rated/movies${TMDB.common.generateQuery(
        options,
      )}`,
    ),
  getRatedTvShows: options =>
    fetch(
      `${TMDB.common.base_uri}account/${options.id}/rated/tv${TMDB.common.generateQuery(options)}`,
    ),
};

TMDB.movie = {
  getDetails: options =>
    fetch(`${TMDB.common.base_uri}movie/${options.id}${TMDB.common.generateQuery(options)}`),
  getAccountStates: options =>
    fetch(
      `${TMDB.common.base_uri}movie/${options.id}/account_states${TMDB.common.generateQuery(
        options,
      )}`,
    ),
  getRecomendations: options =>
    fetch(
      `${TMDB.common.base_uri}movie/${options.id}/recommendations${TMDB.common.generateQuery(
        options,
      )}`,
    ),
  getSimilar: options =>
    fetch(
      `${TMDB.common.base_uri}movie/${options.id}/similar${TMDB.common.generateQuery(options)}`,
    ),
};

TMDB.tv = {
  getDetails: options =>
    fetch(`${TMDB.common.base_uri}tv/${options.id}${TMDB.common.generateQuery(options)}`),
  getAccountStates: options =>
    fetch(
      `${TMDB.common.base_uri}tv/${options.id}/account_states${TMDB.common.generateQuery(options)}`,
    ),
  getRecomendations: options =>
    fetch(
      `${TMDB.common.base_uri}tv/${options.id}/recommendations${TMDB.common.generateQuery(
        options,
      )}`,
    ),
  getSimilar: options =>
    fetch(`${TMDB.common.base_uri}tv/${options.id}/similar${TMDB.common.generateQuery(options)}`),
};

TMDB.tvSeason = {
  getDetails: options =>
    fetch(
      `${TMDB.common.base_uri}tv/${options.id}/season/${
        options.season_number
      }${TMDB.common.generateQuery(options)}`,
    ),
};

TMDB.tvEpisode = {
  getDetails: options =>
    fetch(
      `${TMDB.common.base_uri}tv/${options.id}/season/${options.season_number}/episode/${
        options.episode_number
      }${TMDB.common.generateQuery(options)}`,
    ),
  getImages: options =>
    fetch(
      `${TMDB.common.base_uri}tv/${options.id}/season/${options.season_number}/episode/${
        options.episode_number
      }/images${TMDB.common.generateQuery(options)}`,
    ),
};

export default TMDB;
