import * as ActionTypes from '../actions/tvs';

const singleTVReducer = (
  state = {
    tvID: 0,
    tvDetails: {
      seasons: [],
    },
    accountStates: {
      rated: false,
    },
    tvRecomendations: [],
    tvSimilars: [],
    tvSeason: {
      episodes: [],
    },
    tvEpisode: {
      images: [],
    },
    isFetching: false,
    isError: false,
  },
  action,
) => {
  switch (action.type) {
    case ActionTypes.GET_TV_DETAILS_REQUEST:
      state = {
        ...state,
        isFetching: true,
      };
      break;

    case ActionTypes.GET_TV_DETAILS_FAILURE:
      state = {
        ...state,
        isError: true,
      };
      break;

    case ActionTypes.GET_TV_DETAILS_SUCCESS:
      state = {
        ...state,
        tvID: action.payload.id,
        tvDetails: action.payload,
        isError: false,
        isFetching: false,
      };
      break;

    case ActionTypes.GET_TV_ACC_STATES_REQUEST:
      state = {
        ...state,
        isFetching: true,
      };
      break;

    case ActionTypes.GET_TV_ACC_STATES_FAILURE:
      state = {
        ...state,
        isError: true,
      };
      break;

    case ActionTypes.GET_TV_ACC_STATES_SUCCESS:
      state = {
        ...state,
        accountStates: action.payload,
        isError: false,
        isFetching: false,
      };
      break;

    case ActionTypes.GET_TV_RECOMEND_REQUEST:
      state = {
        ...state,
        isFetching: true,
      };
      break;

    case ActionTypes.GET_TV_RECOMEND_FAILURE:
      state = {
        ...state,
        isError: true,
      };
      break;

    case ActionTypes.GET_TV_RECOMEND_SUCCESS:
      state = {
        ...state,
        tvRecomendations: [...action.payload.results],
        isError: false,
        isFetching: false,
      };
      break;

    case ActionTypes.GET_TV_SIMILAR_REQUEST:
      state = {
        ...state,
        isFetching: true,
      };
      break;

    case ActionTypes.GET_TV_SIMILAR_FAILURE:
      state = {
        ...state,
        isError: true,
      };
      break;

    case ActionTypes.GET_TV_SIMILAR_SUCCESS:
      state = {
        ...state,
        tvSimilars: [...action.payload.results],
        isError: false,
        isFetching: false,
      };
      break;

    case ActionTypes.GET_TV_SEASON_REQUEST:
      state = {
        ...state,
        isFetching: true,
      };
      break;

    case ActionTypes.GET_TV_SEASON_FAILURE:
      state = {
        ...state,
        isError: true,
      };
      break;

    case ActionTypes.GET_TV_SEASON_SUCCESS:
      state = {
        ...state,
        tvSeason: action.payload,
        isError: false,
        isFetching: false,
      };
      break;

    case ActionTypes.GET_TV_EPISODE_REQUEST:
      state = {
        ...state,
        isFetching: true,
      };
      break;

    case ActionTypes.GET_TV_EPISODE_FAILURE:
      state = {
        ...state,
        isError: true,
      };
      break;

    case ActionTypes.GET_TV_EPISODE_SUCCESS:
      state = {
        ...state,
        tvEpisode: {
          ...state.tvEpisode,
          ...action.payload,
        },
        isError: false,
        isFetching: false,
      };
      break;

    case ActionTypes.GET_TV_EPISODE_IMAGES_REQUEST:
      state = {
        ...state,
        tvEpisode: {
          ...state.tvEpisode,
          images: [],
        },
        isFetching: true,
      };
      break;

    case ActionTypes.GET_TV_EPISODE_IMAGES_FAILURE:
      state = {
        ...state,
        isError: true,
      };
      break;

    case ActionTypes.GET_TV_EPISODE_IMAGES_SUCCESS:
      state = {
        ...state,
        tvEpisode: {
          ...state.tvEpisode,
          images: [...action.payload.stills],
        },
        isError: false,
        isFetching: false,
      };
      break;

    default:
      return state;
  }

  return state;
};

export default singleTVReducer;
