import * as ActionTypes from '../actions/movies';

const singleMovieReducer = (
  state = {
    movieID: 0,
    movieDetails: {},
    accountStates: {
      rated: false,
    },
    movieRecomendations: [],
    movieSimilars: [],
    isFetching: false,
    isError: false,
  },
  action,
) => {
  switch (action.type) {
    case ActionTypes.GET_MOVIE_DETAILS_REQUEST:
      state = {
        ...state,
        isFetching: true,
      };
      break;

    case ActionTypes.GET_MOVIE_DETAILS_FAILURE:
      state = {
        ...state,
        isError: true,
      };
      break;

    case ActionTypes.GET_MOVIE_DETAILS_SUCCESS:
      state = {
        ...state,
        movieID: action.payload.id,
        movieDetails: action.payload,
        isError: false,
        isFetching: false,
      };
      break;

    case ActionTypes.GET_MOVIE_ACC_STATES_REQUEST:
      state = {
        ...state,
        isFetching: true,
      };
      break;

    case ActionTypes.GET_MOVIE_ACC_STATES_FAILURE:
      state = {
        ...state,
        isError: true,
      };
      break;

    case ActionTypes.GET_MOVIE_ACC_STATES_SUCCESS:
      state = {
        ...state,
        accountStates: action.payload,
        isError: false,
        isFetching: false,
      };
      break;

    case ActionTypes.GET_MOVIE_RECOMEND_REQUEST:
      state = {
        ...state,
        isFetching: true,
      };
      break;

    case ActionTypes.GET_MOVIE_RECOMEND_FAILURE:
      state = {
        ...state,
        isError: true,
      };
      break;

    case ActionTypes.GET_MOVIE_RECOMEND_SUCCESS:
      state = {
        ...state,
        movieRecomendations: [...action.payload.results],
        isError: false,
        isFetching: false,
      };
      break;

    case ActionTypes.GET_MOVIE_SIMILAR_REQUEST:
      state = {
        ...state,
        isFetching: true,
      };
      break;

    case ActionTypes.GET_MOVIE_SIMILAR_FAILURE:
      state = {
        ...state,
        isError: true,
      };
      break;

    case ActionTypes.GET_MOVIE_SIMILAR_SUCCESS:
      state = {
        ...state,
        movieSimilars: [...action.payload.results],
        isError: false,
        isFetching: false,
      };
      break;

    default:
      return state;
  }

  return state;
};

export default singleMovieReducer;
