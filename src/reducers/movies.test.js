import * as ActionTypes from '../actions/movies';
import moviesReducer from './movies';
import getMoviesMock from '../mocks/getMoviesMock';

const initialState = {
  movies: [],
  page: 1,
  totalResults: 0,
  totalPages: 0,
  sortBy: 'popularity.desc',
  genres: [],
  isFetching: false,
  isError: false,
};

describe('reducers/movies', () => {
  it('should have initial state', () => {
    expect(moviesReducer(undefined, {})).toEqual(initialState);
  });

  it('should handle GET_MOVIES_REQUEST', () => {
    const startAction = {
      type: ActionTypes.GET_MOVIES_REQUEST,
    };
    expect(moviesReducer({}, startAction)).toEqual({
      isFetching: true,
    });
  });

  it('should handle GET_MOVIES_FAILURE', () => {
    const startAction = {
      type: ActionTypes.GET_MOVIES_FAILURE,
    };
    expect(moviesReducer({}, startAction)).toEqual({
      isError: true,
    });
  });

  it('should handle GET_MOVIES_SUCCESS', () => {
    const startAction = {
      type: ActionTypes.GET_MOVIES_SUCCESS,
      payload: getMoviesMock,
    };
    expect(moviesReducer({}, startAction)).toEqual({
      movies: [...getMoviesMock.results],
      page: getMoviesMock.page,
      totalResults: getMoviesMock.total_results,
      totalPages: getMoviesMock.total_pages,
      isError: false,
      isFetching: false,
    });
  });
});
