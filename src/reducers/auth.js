import * as ActionTypes from '../actions/auth';

const authReducer = (
  state = {
    requestTokken: '',
    sessionID: '',
    isLoggedIn: false,
    isFetching: false,
    isError: false,
    errorObj: {},
  },
  action,
) => {
  switch (action.type) {
    case ActionTypes.GET_REQ_TOKEN_REQUEST:
      state = {
        ...state,
        isFetching: true,
      };
      break;

    case ActionTypes.GET_REQ_TOKEN_FAILURE:
      state = {
        ...state,
        isError: true,
        errorObj: action.payload,
      };
      break;

    case ActionTypes.GET_REQ_TOKEN_SUCCESS:
      state = {
        ...state,
        requestTokken: action.payload.request_token,
        isError: false,
        isFetching: false,
      };
      break;

    case ActionTypes.GET_SESSION_ID_REQUEST:
      state = {
        ...state,
        isFetching: true,
      };
      break;

    case ActionTypes.GET_SESSION_ID_FAILURE:
      state = {
        ...state,
        isError: true,
        errorObj: action.payload,
      };
      break;

    case ActionTypes.GET_SESSION_ID_SUCCESS:
      state = {
        ...state,
        sessionID: action.payload.session_id,
        isError: false,
        isFetching: false,
        isLoggedIn: true,
      };
      break;

    case ActionTypes.LOGOUT_USER:
      state = {
        ...state,
        requestTokken: '',
        sessionID: '',
        isError: false,
        isFetching: false,
        isLoggedIn: false,
      };
      break;

    default:
      return state;
  }

  return state;
};

export default authReducer;
