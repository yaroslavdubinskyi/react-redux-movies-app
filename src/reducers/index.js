import { combineReducers } from 'redux';
import moviesReducer from './movies';
import singleMovieReducer from './singleMovie';
import singleTVReducer from './singleTV';
import tvsReducer from './tvs';
import authReducer from './auth';
import userReducer from './user';

const rootReducer = combineReducers({
  auth: authReducer,
  movies: moviesReducer,
  tvShows: tvsReducer,
  singleMovie: singleMovieReducer,
  singleTV: singleTVReducer,
  user: userReducer,
});

export default rootReducer;
