import * as ActionTypes from '../actions/movies';
import singleMovieReducer from './singleMovie';
import getMovieDetailsMock from '../mocks/getMovieDetailsMock';
import getMovieAccStatesMock from '../mocks/getMovieAccStatesMock';
import getMovieRecomendMock from '../mocks/getMovieRecomendMock';
import getMovieSimilarMock from '../mocks/getMovieSimilarMock';

const initialState = {
  movieID: 0,
  movieDetails: {},
  accountStates: {
    rated: false,
  },
  movieRecomendations: [],
  movieSimilars: [],
  isFetching: false,
  isError: false,
};

describe('reducers/singleMovie', () => {
  it('should have initial state', () => {
    expect(singleMovieReducer(undefined, {})).toEqual(initialState);
  });

  it('should handle GET_MOVIE_DETAILS_REQUEST', () => {
    const startAction = {
      type: ActionTypes.GET_MOVIE_DETAILS_REQUEST,
    };
    expect(singleMovieReducer({}, startAction)).toEqual({
      isFetching: true,
    });
  });

  it('should handle GET_MOVIE_DETAILS_FAILURE', () => {
    const startAction = {
      type: ActionTypes.GET_MOVIE_DETAILS_FAILURE,
    };
    expect(singleMovieReducer({}, startAction)).toEqual({
      isError: true,
    });
  });

  it('should handle GET_MOVIE_DETAILS_SUCCESS', () => {
    const startAction = {
      type: ActionTypes.GET_MOVIE_DETAILS_SUCCESS,
      payload: getMovieDetailsMock,
    };
    expect(singleMovieReducer({}, startAction)).toEqual({
      movieID: getMovieDetailsMock.id,
      movieDetails: getMovieDetailsMock,
      isError: false,
      isFetching: false,
    });
  });

  it('should handle GET_MOVIE_ACC_STATES_REQUEST', () => {
    const startAction = {
      type: ActionTypes.GET_MOVIE_ACC_STATES_REQUEST,
    };
    expect(singleMovieReducer({}, startAction)).toEqual({
      isFetching: true,
    });
  });

  it('should handle GET_MOVIE_ACC_STATES_FAILURE', () => {
    const startAction = {
      type: ActionTypes.GET_MOVIE_ACC_STATES_FAILURE,
    };
    expect(singleMovieReducer({}, startAction)).toEqual({
      isError: true,
    });
  });

  it('should handle GET_MOVIE_ACC_STATES_SUCCESS', () => {
    const startAction = {
      type: ActionTypes.GET_MOVIE_ACC_STATES_SUCCESS,
      payload: getMovieAccStatesMock,
    };
    expect(singleMovieReducer({}, startAction)).toEqual({
      accountStates: getMovieAccStatesMock,
      isError: false,
      isFetching: false,
    });
  });

  it('should handle GET_MOVIE_RECOMEND_REQUEST', () => {
    const startAction = {
      type: ActionTypes.GET_MOVIE_RECOMEND_REQUEST,
    };
    expect(singleMovieReducer({}, startAction)).toEqual({
      isFetching: true,
    });
  });

  it('should handle GET_MOVIE_RECOMEND_FAILURE', () => {
    const startAction = {
      type: ActionTypes.GET_MOVIE_RECOMEND_FAILURE,
    };
    expect(singleMovieReducer({}, startAction)).toEqual({
      isError: true,
    });
  });

  it('should handle GET_MOVIE_RECOMEND_SUCCESS', () => {
    const startAction = {
      type: ActionTypes.GET_MOVIE_RECOMEND_SUCCESS,
      payload: getMovieRecomendMock,
    };
    expect(singleMovieReducer({}, startAction)).toEqual({
      movieRecomendations: [...getMovieRecomendMock.results],
      isError: false,
      isFetching: false,
    });
  });

  it('should handle GET_MOVIE_SIMILAR_REQUEST', () => {
    const startAction = {
      type: ActionTypes.GET_MOVIE_SIMILAR_REQUEST,
    };
    expect(singleMovieReducer({}, startAction)).toEqual({
      isFetching: true,
    });
  });

  it('should handle GET_MOVIE_SIMILAR_FAILURE', () => {
    const startAction = {
      type: ActionTypes.GET_MOVIE_SIMILAR_FAILURE,
    };
    expect(singleMovieReducer({}, startAction)).toEqual({
      isError: true,
    });
  });

  it('should handle GET_MOVIE_SIMILAR_SUCCESS', () => {
    const startAction = {
      type: ActionTypes.GET_MOVIE_SIMILAR_SUCCESS,
      payload: getMovieSimilarMock,
    };
    expect(singleMovieReducer({}, startAction)).toEqual({
      movieSimilars: [...getMovieSimilarMock.results],
      isError: false,
      isFetching: false,
    });
  });
});
