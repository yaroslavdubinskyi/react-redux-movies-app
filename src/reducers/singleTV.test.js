import * as ActionTypes from '../actions/tvs';
import singleTVReducer from './singleTV';
import getTvDetailsMock from '../mocks/getTvDetailsMock';
import getTvAccStatesMock from '../mocks/getTvAccStatesMock';
import getTvRecomendMock from '../mocks/getTvRecomendMock';
import getTvSimilarMock from '../mocks/getTvSimilarMock';
import getTvSeasonMock from '../mocks/getTvSeasonMock';
import getTvEpisodeMock from '../mocks/getTvEpisodeMock';
import getTvEpisodeImages from '../mocks/getTvEpisodeImages';

const initialState = {
  tvID: 0,
  tvDetails: {
    seasons: [],
  },
  accountStates: {
    rated: false,
  },
  tvRecomendations: [],
  tvSimilars: [],
  tvSeason: {
    episodes: [],
  },
  tvEpisode: {
    images: [],
  },
  isFetching: false,
  isError: false,
};

describe('reducers/singleTV', () => {
  it('should have initial state', () => {
    expect(singleTVReducer(undefined, {})).toEqual(initialState);
  });

  it('should handle GET_TV_DETAILS_REQUEST', () => {
    const startAction = {
      type: ActionTypes.GET_TV_DETAILS_REQUEST,
    };
    expect(singleTVReducer({}, startAction)).toEqual({
      isFetching: true,
    });
  });

  it('should handle GET_TV_DETAILS_FAILURE', () => {
    const startAction = {
      type: ActionTypes.GET_TV_DETAILS_FAILURE,
    };
    expect(singleTVReducer({}, startAction)).toEqual({
      isError: true,
    });
  });

  it('should handle GET_TV_DETAILS_SUCCESS', () => {
    const startAction = {
      type: ActionTypes.GET_TV_DETAILS_SUCCESS,
      payload: getTvDetailsMock,
    };
    expect(singleTVReducer({}, startAction)).toEqual({
      tvID: getTvDetailsMock.id,
      tvDetails: getTvDetailsMock,
      isError: false,
      isFetching: false,
    });
  });

  it('should handle GET_TV_ACC_STATES_REQUEST', () => {
    const startAction = {
      type: ActionTypes.GET_TV_ACC_STATES_REQUEST,
    };
    expect(singleTVReducer({}, startAction)).toEqual({
      isFetching: true,
    });
  });

  it('should handle GET_TV_ACC_STATES_FAILURE', () => {
    const startAction = {
      type: ActionTypes.GET_TV_ACC_STATES_FAILURE,
    };
    expect(singleTVReducer({}, startAction)).toEqual({
      isError: true,
    });
  });

  it('should handle GET_TV_ACC_STATES_SUCCESS', () => {
    const startAction = {
      type: ActionTypes.GET_TV_ACC_STATES_SUCCESS,
      payload: getTvAccStatesMock,
    };
    expect(singleTVReducer({}, startAction)).toEqual({
      accountStates: getTvAccStatesMock,
      isError: false,
      isFetching: false,
    });
  });

  it('should handle GET_TV_RECOMEND_REQUEST', () => {
    const startAction = {
      type: ActionTypes.GET_TV_RECOMEND_REQUEST,
    };
    expect(singleTVReducer({}, startAction)).toEqual({
      isFetching: true,
    });
  });

  it('should handle GET_TV_RECOMEND_FAILURE', () => {
    const startAction = {
      type: ActionTypes.GET_TV_RECOMEND_FAILURE,
    };
    expect(singleTVReducer({}, startAction)).toEqual({
      isError: true,
    });
  });

  it('should handle GET_TV_RECOMEND_SUCCESS', () => {
    const startAction = {
      type: ActionTypes.GET_TV_RECOMEND_SUCCESS,
      payload: getTvRecomendMock,
    };
    expect(singleTVReducer({}, startAction)).toEqual({
      tvRecomendations: [...getTvRecomendMock.results],
      isError: false,
      isFetching: false,
    });
  });

  it('should handle GET_TV_SIMILAR_REQUEST', () => {
    const startAction = {
      type: ActionTypes.GET_TV_SIMILAR_REQUEST,
    };
    expect(singleTVReducer({}, startAction)).toEqual({
      isFetching: true,
    });
  });

  it('should handle GET_TV_SIMILAR_FAILURE', () => {
    const startAction = {
      type: ActionTypes.GET_TV_SIMILAR_FAILURE,
    };
    expect(singleTVReducer({}, startAction)).toEqual({
      isError: true,
    });
  });

  it('should handle GET_TV_SIMILAR_SUCCESS', () => {
    const startAction = {
      type: ActionTypes.GET_TV_SIMILAR_SUCCESS,
      payload: getTvSimilarMock,
    };
    expect(singleTVReducer({}, startAction)).toEqual({
      tvSimilars: [...getTvSimilarMock.results],
      isError: false,
      isFetching: false,
    });
  });

  it('should handle GET_TV_SEASON_REQUEST', () => {
    const startAction = {
      type: ActionTypes.GET_TV_SEASON_REQUEST,
    };
    expect(singleTVReducer({}, startAction)).toEqual({
      isFetching: true,
    });
  });

  it('should handle GET_TV_SEASON_FAILURE', () => {
    const startAction = {
      type: ActionTypes.GET_TV_SEASON_FAILURE,
    };
    expect(singleTVReducer({}, startAction)).toEqual({
      isError: true,
    });
  });

  it('should handle GET_TV_SEASON_SUCCESS', () => {
    const startAction = {
      type: ActionTypes.GET_TV_SEASON_SUCCESS,
      payload: getTvSeasonMock,
    };
    expect(singleTVReducer({}, startAction)).toEqual({
      tvSeason: getTvSeasonMock,
      isError: false,
      isFetching: false,
    });
  });

  it('should handle GET_TV_EPISODE_REQUEST', () => {
    const startAction = {
      type: ActionTypes.GET_TV_EPISODE_REQUEST,
    };
    expect(singleTVReducer({}, startAction)).toEqual({
      isFetching: true,
    });
  });

  it('should handle GET_TV_EPISODE_FAILURE', () => {
    const startAction = {
      type: ActionTypes.GET_TV_EPISODE_FAILURE,
    };
    expect(singleTVReducer({}, startAction)).toEqual({
      isError: true,
    });
  });

  it('should handle GET_TV_EPISODE_SUCCESS', () => {
    const startAction = {
      type: ActionTypes.GET_TV_EPISODE_SUCCESS,
      payload: getTvEpisodeMock,
    };
    expect(singleTVReducer({}, startAction)).toEqual({
      tvEpisode: {
        ...getTvEpisodeMock,
      },
      isError: false,
      isFetching: false,
    });
  });

  it('should handle GET_TV_EPISODE_IMAGES_REQUEST', () => {
    const startAction = {
      type: ActionTypes.GET_TV_EPISODE_IMAGES_REQUEST,
    };
    expect(singleTVReducer({}, startAction)).toEqual({
      tvEpisode: {
        images: [],
      },
      isFetching: true,
    });
  });

  it('should handle GET_TV_EPISODE_IMAGES_FAILURE', () => {
    const startAction = {
      type: ActionTypes.GET_TV_EPISODE_IMAGES_FAILURE,
    };
    expect(singleTVReducer({}, startAction)).toEqual({
      isError: true,
    });
  });

  it('should handle GET_TV_EPISODE_IMAGES_SUCCESS', () => {
    const startAction = {
      type: ActionTypes.GET_TV_EPISODE_IMAGES_SUCCESS,
      payload: getTvEpisodeImages,
    };
    expect(singleTVReducer({}, startAction)).toEqual({
      tvEpisode: {
        images: [...getTvEpisodeImages.stills],
      },
      isError: false,
      isFetching: false,
    });
  });
});
