import * as ActionTypes from '../actions/movies';

const moviesReducer = (
  state = {
    movies: [],
    page: 1,
    totalResults: 0,
    totalPages: 0,
    sortBy: 'popularity.desc',
    genres: [],
    isFetching: false,
    isError: false,
  },
  action,
) => {
  switch (action.type) {
    case ActionTypes.GET_MOVIES_REQUEST:
      state = {
        ...state,
        isFetching: true,
      };
      break;

    case ActionTypes.GET_MOVIES_FAILURE:
      state = {
        ...state,
        isError: true,
      };
      break;

    case ActionTypes.GET_MOVIES_SUCCESS:
      state = {
        ...state,
        movies: [...action.payload.results],
        page: action.payload.page,
        totalResults: action.payload.total_results,
        totalPages: action.payload.total_pages,
        isError: false,
        isFetching: false,
      };
      break;

    default:
      return state;
  }

  return state;
};

export default moviesReducer;
