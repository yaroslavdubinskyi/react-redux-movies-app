import * as ActionTypes from '../actions/user';
import { LOGOUT_USER } from '../actions/auth';

const userReducer = (
  state = {
    userID: '',
    userDetails: {
      avatar: {
        gravatar: {
          hash: '',
        },
      },
    },
    ratedMovies: [],
    ratedTvShows: [],
    ratedTvEpisodes: [],
    favoriteMovies: [],
    favoriteTvShows: [],
    isFetching: false,
    isError: false,
    errorObj: {},
  },
  action,
) => {
  switch (action.type) {
    case ActionTypes.GET_USER_DETAILS_REQUEST:
      state = {
        ...state,
        isFetching: true,
      };
      break;

    case ActionTypes.GET_USER_DETAILS_FAILURE:
      state = {
        ...state,
        isError: true,
        errorObj: action.payload,
      };
      break;

    case ActionTypes.GET_USER_DETAILS_SUCCESS:
      state = {
        ...state,
        userID: action.payload.id,
        userDetails: action.payload,
        isError: false,
        isFetching: false,
      };
      break;

    case ActionTypes.GET_USER_RATED_MOVIES_REQUEST:
      state = {
        ...state,
        isFetching: true,
      };
      break;

    case ActionTypes.GET_USER_RATED_MOVIES_FAILURE:
      state = {
        ...state,
        isError: true,
        errorObj: action.payload,
      };
      break;

    case ActionTypes.GET_USER_RATED_MOVIES_SUCCESS:
      state = {
        ...state,
        ratedMovies: [...action.payload.results],
        isError: false,
        isFetching: false,
      };
      break;

    case ActionTypes.GET_USER_RATED_TV_REQUEST:
      state = {
        ...state,
        isFetching: true,
      };
      break;

    case ActionTypes.GET_USER_RATED_TV_FAILURE:
      state = {
        ...state,
        isError: true,
        errorObj: action.payload,
      };
      break;

    case ActionTypes.GET_USER_RATED_TV_SUCCESS:
      state = {
        ...state,
        ratedTvShows: [...action.payload.results],
        isError: false,
        isFetching: false,
      };
      break;

    case ActionTypes.GET_USER_FAV_MOVIES_REQUEST:
      state = {
        ...state,
        isFetching: true,
      };
      break;

    case ActionTypes.GET_USER_FAV_MOVIES_FAILURE:
      state = {
        ...state,
        isError: true,
        errorObj: action.payload,
      };
      break;

    case ActionTypes.GET_USER_FAV_MOVIES_SUCCESS:
      state = {
        ...state,
        favoriteMovies: [...action.payload.results],
        isError: false,
        isFetching: false,
      };
      break;

    case ActionTypes.GET_USER_FAV_TV_REQUEST:
      state = {
        ...state,
        isFetching: true,
      };
      break;

    case ActionTypes.GET_USER_FAV_TV_FAILURE:
      state = {
        ...state,
        isError: true,
        errorObj: action.payload,
      };
      break;

    case ActionTypes.GET_USER_FAV_TV_SUCCESS:
      state = {
        ...state,
        favoriteTvShows: [...action.payload.results],
        isError: false,
        isFetching: false,
      };
      break;

    case LOGOUT_USER:
      state = {
        ...state,
        userID: '',
        userDetails: {
          avatar: {
            gravatar: {
              hash: '',
            },
          },
        },
        ratedMovies: [],
        ratedTvShows: [],
        ratedTvEpisodes: [],
        favoriteMovies: [],
        favoriteTvShows: [],
        isFetching: false,
        isError: false,
        errorObj: {},
      };
      break;

    default:
      return state;
  }

  return state;
};

export default userReducer;
