import * as ActionTypes from '../actions/tvs';
import tvsReducer from './tvs';
import getTvsMock from '../mocks/getTvsMock';

const initialState = {
  tvShows: [],
  page: 1,
  totalResults: 0,
  totalPages: 0,
  sortBy: 'popularity.desc',
  genres: [],
  isFetching: false,
  isError: false,
};

describe('reducers/tvs', () => {
  it('should have initial state', () => {
    expect(tvsReducer(undefined, {})).toEqual(initialState);
  });

  it('should handle GET_TVS_REQUEST', () => {
    const startAction = {
      type: ActionTypes.GET_TVS_REQUEST,
    };
    expect(tvsReducer({}, startAction)).toEqual({
      isFetching: true,
    });
  });

  it('should handle GET_TVS_FAILURE', () => {
    const startAction = {
      type: ActionTypes.GET_TVS_FAILURE,
    };
    expect(tvsReducer({}, startAction)).toEqual({
      isError: true,
    });
  });

  it('should handle GET_TVS_SUCCESS', () => {
    const startAction = {
      type: ActionTypes.GET_TVS_SUCCESS,
      payload: getTvsMock,
    };
    expect(tvsReducer({}, startAction)).toEqual({
      tvShows: [...getTvsMock.results],
      page: getTvsMock.page,
      totalResults: getTvsMock.total_results,
      totalPages: getTvsMock.total_pages,
      isError: false,
      isFetching: false,
    });
  });
});
